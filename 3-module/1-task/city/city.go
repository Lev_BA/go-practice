package city

import (
	"gopackages/wordz"
)

func City() string{
	wordz.Words = []string{"Moscow", "Kaluga", "Omsk", "Chelyabinsk"}
	return wordz.Random()
}