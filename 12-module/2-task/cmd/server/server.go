package main

import (
	"fmt"
	"google.golang.org/grpc"
	"mod12task2/internal/handlers"
	pkg "mod12task2/pkg/api/users.proto"
	"net"
)

func main() {
	fmt.Println("Starting server ...")
	lis, err := net.Listen("tcp", ":9999")
	if err != nil {
		panic(err)
	}
	s := grpc.NewServer()
	server := &handlers.Server{}
	pkg.RegisterUsersServer(s, server)
	if err := s.Serve(lis); err != nil {
		panic(err)
	}
}
