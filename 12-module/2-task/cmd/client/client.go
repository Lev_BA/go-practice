package main

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	pkg "mod12task2/pkg/api/users.proto"
	"time"
)

func main() {
	cwt, _ := context.WithTimeout(context.Background(), time.Second*5)
	conn, err := grpc.DialContext(cwt, "localhost:9999", grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		panic(err)
	}
	defer conn.Close()

	uc := pkg.NewUsersClient(conn)

	user := &pkg.User{
		Email:     "lev@lev.ru",
		FirstName: "Lev",
		LastName:  "Orlov",
		Password:  "12345",
	}

	us, err := uc.CreateUser(cwt, user)
	if err != nil {
		panic(err)
	}

	if us.Success {
		fmt.Println("User successfully created")
	}

	users, err := uc.GetUsers(cwt, &pkg.Filter{})
	if err != nil {
		panic(err)
	}
	fmt.Println(users)
}
