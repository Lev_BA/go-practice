package handlers

import (
	"context"
	"errors"
	pkg "mod12task2/pkg/api/users.proto"
)

type Server struct {
	Users []*pkg.User
	pkg.UnimplementedUsersServer
}

func (s *Server) CreateUser(ctx context.Context, user *pkg.User) (*pkg.UserState, error) {
	if user.Email == "" || user.FirstName == "" || user.LastName == "" || user.Password == "" {
		return &pkg.UserState{Success: false}, errors.New("Wrong user")
	}
	s.Users = append(s.Users, user)

	return &pkg.UserState{Success: true}, nil
}

func (s *Server) GetUsers(ctx context.Context, filter *pkg.Filter) (*pkg.UsersList, error) {
	return &pkg.UsersList{List: s.Users}, nil
}
