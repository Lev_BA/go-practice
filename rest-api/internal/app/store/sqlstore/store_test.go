package sqlstore_test

import (
	"os"
	"testing"
)

var (
	databaseURL string
)

func TestMain(m *testing.M) {
	databaseURL = os.Getenv("DATABASE_URL")
	if databaseURL == "" {
		databaseURL = "host=localhost user=postgres password=1111 dbname=test-rest-api port=5432 sslmode=disable TimeZone=Asia/Shanghai"
	}

	os.Exit(m.Run())
}
