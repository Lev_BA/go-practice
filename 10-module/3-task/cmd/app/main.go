package main

import (
	"fmt"
)

func HelloSystem(system string) string {
	return fmt.Sprintf("Hello %s", system)
}

func main() {
	fmt.Println(HelloSystem(SystemName))
}
