package main

import (
	"fmt"
	"unsafe"
)

type Lead struct {
	Name   string
	Age    int16
	Active bool
	Budget int
	Items  []int
}

func main() {
	lead := Lead{
		Name:   "Alex",
		Age:    30,
		Active: true,
		Budget: 10000,
		Items:  []int{1, 2, 3, 4, 5},
	}

	dfg := *(*uintptr)(unsafe.Pointer(&lead.Budget)) + 10000

	fmt.Println(dfg)

	//aa = (*int)(unsafe.Pointer(uintptr(unsafe.Pointer(aa)) + 1))

	//budget := (*[size]byte)(unsafe.Pointer(&lead.Budget))

	//budget1 := uintptr(unsafe.Pointer(&lead.Budget))

	f := unsafe.Pointer(uintptr(unsafe.Pointer(&lead)) + unsafe.Offsetof(lead.Budget))

	fmt.Printf("%#v\n", f)

	/*fmt.Printf("Name: Offsetof: %d Sizeof %d Alignof: %d\n",
		unsafe.Offsetof(lead.Name),
		unsafe.Sizeof(lead.Name),
		unsafe.Alignof(lead.Name),
	)

	fmt.Printf("Budget: Offsetof: %d Sizeof %d Alignof: %d\n",
		unsafe.Offsetof(lead.Budget),
		unsafe.Sizeof(lead.Budget),
		unsafe.Alignof(lead.Budget),
	)
	fmt.Printf("Items: Offsetof: %d Sizeof %d Alignof: %d\n",
		unsafe.Offsetof(lead.Items),
		unsafe.Sizeof(lead.Items),
		unsafe.Alignof(lead.Items),
	)

	fmt.Printf("Active: Offsetof: %d Sizeof %d Alignof: %d\n",
		unsafe.Offsetof(lead.Active),
		unsafe.Sizeof(lead.Active),
		unsafe.Alignof(lead.Active),
	)
	fmt.Printf("Age: Offsetof: %d Sizeof %d Alignof: %d\n",
		unsafe.Offsetof(lead.Age),
		unsafe.Sizeof(lead.Age),
		unsafe.Alignof(lead.Age),
	)

	fmt.Printf("Align struct: %d\n", unsafe.Alignof(lead))
	fmt.Printf("Struct Sizeof: %d\n", unsafe.Sizeof(lead))*/

	/*ints := []int16{4, 5, 6, 7, 1, 2, 3, 53}
	lenInts := len(ints)
	capInts := cap(ints)

	pt := unsafe.Pointer(&ints[0])

	ints = append(ints, 77)

	n := (*int16)(unsafe.Pointer(uintptr(unsafe.Pointer(&ints[0])) + unsafe.Sizeof(ints[0])*2))
	*n = 14

	fmt.Println(ints)

	sliceHeader := &reflect.SliceHeader{
		Data: uintptr(pt),
		Len:  lenInts,
		Cap:  capInts,
	}

	oldInts := *(*[]int16)(unsafe.Pointer(sliceHeader))
	fmt.Println(oldInts)*/
}

/*type Customer struct {
	Name     string
	Age      int16
	Discount bool
	Balance  int
	Debt     int
}

func main() {
	customer := Customer{
		Name:     "New customer",
		Balance:  10000,
		Age:      25,
		Discount: true,
		Debt:     3000,
	}

	fmt.Printf("Bytes %#v\n", (*[unsafe.Sizeof(Customer{})]byte)(unsafe.Pointer(&customer)))

	fmt.Printf("Name: Offsetof: %d Sizeof %d Alignof: %d\n",
		unsafe.Offsetof(customer.Name),
		unsafe.Sizeof(customer.Name),
		unsafe.Alignof(customer.Name),
	)
	fmt.Printf("Age: Offsetof: %d Sizeof %d Alignof: %d\n",
		unsafe.Offsetof(customer.Age),
		unsafe.Sizeof(customer.Age),
		unsafe.Alignof(customer.Age),
	)
	fmt.Printf("Balance: Offsetof: %d Sizeof %d Alignof: %d\n",
		unsafe.Offsetof(customer.Balance),
		unsafe.Sizeof(customer.Balance),
		unsafe.Alignof(customer.Balance),
	)
	fmt.Printf("Debt: Offsetof: %d Sizeof %d Alignof: %d\n",
		unsafe.Offsetof(customer.Debt),
		unsafe.Sizeof(customer.Debt),
		unsafe.Alignof(customer.Debt),
	)
	fmt.Printf("Discount: Offsetof: %d Sizeof %d Alignof: %d\n",
		unsafe.Offsetof(customer.Discount),
		unsafe.Sizeof(customer.Discount),
		unsafe.Alignof(customer.Discount),
	)

	fmt.Printf("Align struct: %d\n", unsafe.Alignof(customer))
	fmt.Printf("Struct Sizeof: %d\n", unsafe.Sizeof(customer))
}*/

//st := St{
//	5,
//	0.1,
//	1,
//}
//
//bytes := (*[size]byte)(unsafe.Pointer(&st))
//st1 := (*St)(unsafe.Pointer(&bytes[0]))
//
//fmt.Printf("Origin struct: %#v\n", st)
//fmt.Printf("Bytes: %#v\n", bytes)
//fmt.Printf("Struct: %#v\n", *st1)

//str1 := "some text"
//str2 := "more some text"
//
//fmt.Println("Sizeof str1:", unsafe.Sizeof(str1))
//fmt.Println("Sizeof str2:", unsafe.Sizeof(str2))
//fmt.Println("Len str1:", len(str1))
//fmt.Println("Len str2:", len(str2))
//
//sl1 := []int16{1, 2, 3}
//sl2 := []int16{4, 5, 6, 7, 12}
//
//fmt.Println("Sizeof sl1:", unsafe.Sizeof(sl1))
//fmt.Println("Sizeof sl2:", unsafe.Sizeof(sl2))
//fmt.Println("Len sl1:", len(sl1))
//fmt.Println("Len sl2:", len(sl2))
