package main

import (
	"fmt"
	v1 "gitlab.com/Lev_BA/version"
	v2 "gitlab.com/Lev_BA/version/v2"
)

func main() {
	v1.Version()
	fmt.Println(v2.Version())
}
