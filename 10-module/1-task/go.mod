module mod10task1

go 1.18

require (
	gitlab.com/Lev_BA/version v1.0.3
	gitlab.com/Lev_BA/version/v2 v2.0.0
)

replace gitlab.com/Lev_BA/version/v2 => ./version
