package main

import (
	"fmt"
	"net/http"
	"time"
)

type Handler interface {
	ServeHTTP(w http.ResponseWriter, r *http.Request)
}

type ResponseHandler struct {
	message string
}

func (rh ResponseHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, rh.message)
}

func Hello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Hello human!")
}

func Dir(w http.ResponseWriter, r *http.Request) {
	http.FileServer(http.Dir("./"))
}

func MyTimeout(w http.ResponseWriter, r *http.Request) {
	http.TimeoutHandler(nil, time.Second*10, "Request Timeout")
}

func main() {
	http.HandleFunc("/hello", Hello)

	http.HandleFunc("/source", Dir)

	http.HandleFunc("/long-ping", MyTimeout)

	http.ListenAndServe(":8080", nil)
}
