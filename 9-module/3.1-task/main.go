package main

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
)

func main() {
	Makerequest()

}

func Makerequest() {
	message := map[string]interface{}{
		"name":  "Lev",
		"email": "bryuzgin@uyandex.ru",
		"age":   30,
	}

	bytesRepresentation, err := json.Marshal(message)
	if err != nil {
		log.Fatalln(err)
	}

	resp, err := http.Post("http://localhost:8181/user/", "application/json", bytes.NewBuffer(bytesRepresentation))
	if err != nil {
		log.Fatalln(err)
	}

	var result map[string]interface{}

	json.NewDecoder(resp.Body).Decode(&result)

	log.Println(result)
	log.Println(result["data"])
}
