package user

import (
	"9mod4task/models"
	"log"
	"net/http"
	"strconv"
	"strings"
)

func DeleteUser(w http.ResponseWriter, r *http.Request) {

	elem, err := strconv.Atoi(strings.Split(r.RequestURI, "/")[3])
	if err != nil {
		log.Fatalln(err)
	}
	if elem < len(Users) {
		w.WriteHeader(http.StatusBadRequest)
	}

	copy(Users[elem:], Users[elem+1:])
	Users[len(Users)-1] = models.User{}
	Users = Users[:len(Users)-1]

	w.WriteHeader(http.StatusOK)
}
