package user

import (
	"encoding/json"
	"log"
	"net/http"
)

func GetUsersList(w http.ResponseWriter, r *http.Request) {
	data := make(map[string]interface{}, 0)

	for _, v := range Users {
		data["name"] = v.Name
		data["email"] = v.Email
		data["age"] = v.Age
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		log.Fatalln(err)
	}

	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(jsonData)
}
