package user

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"strings"
)



func GetUserById(w http.ResponseWriter, r *http.Request) {
	elem, err := strconv.Atoi(strings.Split(r.RequestURI, "/")[3])
	if err != nil {
		log.Fatalln(err)
	}
	if elem < len(Users) {
		w.WriteHeader(http.StatusBadRequest)
	}

	data := make(map[string]interface{})

	data["name"] = Users[elem].Name
	data["email"] = Users[elem].Email
	data["age"] = Users[elem].Age

	jsonData, err := json.Marshal(data)
	if err != nil {
		log.Fatalln(err)
	}

	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(jsonData)
}
