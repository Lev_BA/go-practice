package user

import (
	"9mod4task/models"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

var (
	User  models.User
	Users []models.User
)

func CreateUser(w http.ResponseWriter, r *http.Request) {

	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatalln(err)
	}

	err = json.Unmarshal(data, &User)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Fatalln(err)
	}

	Users = append(Users, User)

	w.WriteHeader(http.StatusCreated)
}
