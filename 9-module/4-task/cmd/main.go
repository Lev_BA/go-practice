package main

import (
	"9mod4task/internal/handlers/user"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"net/http"
)

func main() {
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Use(middleware.RequestID)
	r.Route("/user", func(r chi.Router) {
		r.Post("/", user.CreateUser)
		r.Get("/", user.GetUsersList)
		r.Get("/{id}", user.GetUserById)
		r.Delete("/{id}", user.DeleteUser)
	})

	fmt.Println("Server is listening...")
	http.ListenAndServe(":8282", r)
}




