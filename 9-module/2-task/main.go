package main

import (
	"fmt"
	"net/http"
)

func main() {
	http.HandleFunc("/order/status", func(w http.ResponseWriter, r *http.Request) {
		orderIds, ok := r.URL.Query()["ids[]"]
		if !ok {
			panic("id not found")
		}

		for _, v := range orderIds {
			status := orderStatus(v)
			fmt.Fprintf(w, status)
		}

	})

	http.HandleFunc("/send/email", func(w http.ResponseWriter, r *http.Request) {
		email := r.FormValue("email")
		message := r.FormValue("message")

		fmt.Fprint(w, message)
		fmt.Fprint(w, email)
	})

	fmt.Println("Server is listening...")

	http.ListenAndServe(":8181", nil)
}

func orderStatus(id string) string {
	if id == "1" {
		return "active"
	}

	if id == "2" {
		return "pending"
	}

	if id == "3" {
		return "cancel"
	}
	return "not found"
}
