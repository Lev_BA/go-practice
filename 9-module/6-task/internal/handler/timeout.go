package handler

import (
	"context"
	"net/http"
	"time"
)

func TimeoutMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx, _ := context.WithTimeout(r.Context(), 2*time.Second)

		go next.ServeHTTP(w, r.WithContext(ctx))

		for {
			select {
			case <-ctx.Done():
				http.Error(w, "request timeout", http.StatusRequestTimeout)
				return
			}
		}
	})
}

func LongHandler(w http.ResponseWriter, r *http.Request) {
	time.Sleep(3 * time.Second)
	_, _ = w.Write([]byte("execute handler\n"))
}
