package handler

import (
	"context"
	"log"
	"net/http"
	"os"
)

type RequestIDUser struct {
	Name  string
	Age   int
	Email string
	Guid  int
}

var RequestIDUsers = map[string]RequestIDUser{
	"aaa": {
		Name:  "Ivan",
		Age:   33,
		Email: "ivan@yandex.ru",
		Guid:  os.Getuid(),
	},
}

func RequestIDMiddleware(next http.Handler) (http.Handler, int) {
	userID := os.Getuid()
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		u, ok := RequestIDUsers["aaa"]
		if !ok {
			log.Fatal("Error")
		}
		ctx := context.WithValue(r.Context(), userID, u.Guid)
		next.ServeHTTP(w, r.WithContext(ctx))
	}), userID
}

func CreateUserHandler(w http.ResponseWriter, r *http.Request) {
	us := os.Getuid()
	userID := r.Context().Value(us).(string)
	_, _ = w.Write([]byte(userID))
}
