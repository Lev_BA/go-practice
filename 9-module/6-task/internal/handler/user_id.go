package handler

import (
	"context"
	"net/http"
)

const (
	userIDKey = "user_identifier"
)

type User struct {
	ID       string
	Password string
}

var Users = map[string]User{
	"i.shv": {
		ID:       "user.1234",
		Password: "1111",
	},
}

func UserIDMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		name, pass, ok := r.BasicAuth()
		if !ok {
			http.Error(w, "basic auth info not found", http.StatusBadRequest)
			return
		}

		u, ok := Users[name]
		if !ok {
			http.Error(w, "users not found", http.StatusBadRequest)
		}

		if pass != u.Password {
			http.Error(w, "users not found", http.StatusBadRequest)
		}

		ctx := context.WithValue(r.Context(), userIDKey, u.ID)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func Handler(w http.ResponseWriter, r *http.Request) {
	userID := r.Context().Value(userIDKey).(string)
	_, _ = w.Write([]byte(userID))
}
