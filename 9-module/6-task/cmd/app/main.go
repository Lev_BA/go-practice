package main

import (
	"9mod6task/internal/handler"
	"net/http"
)

func main() {
	http.Handle("/user_id", handler.UserIDMiddleware(http.HandlerFunc(handler.Handler)))
	http.Handle("/timeout", handler.TimeoutMiddleware(http.HandlerFunc(handler.LongHandler)))
	http.Handle("/timeout", handler.RequestIDMiddleware(http.HandlerFunc(handler.CreateUserHandler)))
	_ = http.ListenAndServe(":8080", nil)
}
