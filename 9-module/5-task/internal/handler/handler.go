package handler

import (
	"9mod5task/models"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
)

func CreateUser(w http.ResponseWriter, r *http.Request) {
	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatalln(err)
	}

	err = json.Unmarshal(data, &models.MyUser)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Fatalln(err)
	}

	models.MyUsers = append(models.MyUsers, models.MyUser)

	w.WriteHeader(http.StatusCreated)
}

func DeleteUser(w http.ResponseWriter, r *http.Request) {
	elem, err := strconv.Atoi(strings.Split(r.RequestURI, "/")[3])
	if err != nil {
		log.Fatalln(err)
	}
	if elem < len(models.MyUsers) {
		w.WriteHeader(http.StatusBadRequest)
	}

	copy(models.MyUsers[elem:], models.MyUsers[elem+1:])
	models.MyUsers[len(models.MyUsers)-1] = models.User{}
	models.MyUsers = models.MyUsers[:len(models.MyUsers)-1]

	w.WriteHeader(http.StatusOK)
}
