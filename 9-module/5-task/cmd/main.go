package main

import (
	"9mod5task/internal/handler"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

type User struct {
	Name     string
	Password string
	Active   bool
}

func NewUser(name, password string, active bool) *User {
	return &User{
		Name:     name,
		Password: password,
		Active:   active,
	}
}

func main() {
	u := NewUser("Lev", "1111", true)
	c := mux.NewRouter()

	c.HandleFunc("/", handleHome)
	c.HandleFunc("/login", handleRequest)
	c.Handle("/new", CreateUserMiddleware(u, http.HandlerFunc(handler.CreateUser)))
	c.Handle("/delete", DeleteUserMiddleware(u, http.HandlerFunc(handler.DeleteUser)))
	http.Handle("/", c)

	fmt.Println("Server...")
	http.ListenAndServe(":8080", nil)
}

func handleHome(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func handleRequest(w http.ResponseWriter, r *http.Request) {
	u := NewUser("Lev", "1111", true)

	us, pass, ok := r.BasicAuth()
	if !ok {
		log.Fatal("Error parsing basic auth")
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	if us != u.Name {
		log.Printf("Username provided is: %v", us)
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	if pass != u.Password {
		log.Printf("PAssword provided is: %v", pass)
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	fmt.Println(us)
	fmt.Println(pass)
	w.WriteHeader(http.StatusOK)
	return
}

func DeleteUserMiddleware(u *User, next http.Handler) http.Handler {
	if u.Active == false {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			fmt.Println("Нет доступа")
			w.WriteHeader(http.StatusUnauthorized)
		})
	}
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("Удаление пользователя")
		next.ServeHTTP(w, r)
	})
}

func CreateUserMiddleware(u *User, next http.Handler) http.Handler {
	if u.Active == false {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			fmt.Println("Нет доступа")
			w.WriteHeader(http.StatusUnauthorized)
		})
	}
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("Создание пользователя")
		next.ServeHTTP(w, r)
	})
}

//func Handler(w http.ResponseWriter, r *http.Request) {
//	log.Println("this is Middleware")
//	w.WriteHeader(http.StatusOK)
//}
//
//func Middleware(next http.HandlerFunc) http.HandlerFunc {
//	return func(w http.ResponseWriter, r *http.Request) {
//		log.Println("Hello from middleware")
//		next(w, r)
//	}
//}
