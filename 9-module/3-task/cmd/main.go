package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
)

type User struct {
	Name  string `json:"name"`
	Email string `json:"email"`
	Age   int    `json:"age"`
}

var (
	user  User
	users []User
)

func main() {
	r := mux.NewRouter()

	v1router := r.PathPrefix("/user").Subrouter()
	v1router.HandleFunc("/", createUser).Methods("POST")
	v1router.HandleFunc("/", getUsersList).Methods("GET")
	v1router.HandleFunc("/{id:[0-9]+}", getUserById).Methods("GET")
	v1router.HandleFunc("/{id:[0-9]+}", deleteUser).Methods("DELETE")
	http.Handle("/", r)

	fmt.Println("Server is listening...")
	http.ListenAndServe(":8181", nil)
}

func createUser(w http.ResponseWriter, r *http.Request) {

	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatalln(err)
	}

	err = json.Unmarshal(data, &user)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Fatalln(err)
	}

	users = append(users, user)

	w.WriteHeader(http.StatusCreated)
}

func getUsersList(w http.ResponseWriter, r *http.Request) {
	data := make(map[string]interface{})

	for _, v := range users {
		data["name"] = v.Name
		data["email"] = v.Email
		data["age"] = v.Age
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		log.Fatalln(err)
	}

	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(jsonData)
}

func getUserById(w http.ResponseWriter, r *http.Request) {
	elem, err := strconv.Atoi(strings.Split(r.RequestURI, "/")[3])
	if err != nil {
		log.Fatalln(err)
	}
	if elem < len(users) {
		w.WriteHeader(http.StatusBadRequest)
	}

	data := make(map[string]interface{})

	data["name"] = users[elem].Name
	data["email"] = users[elem].Email
	data["age"] = users[elem].Age

	jsonData, err := json.Marshal(data)
	if err != nil {
		log.Fatalln(err)
	}

	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(jsonData)
}

func deleteUser(w http.ResponseWriter, r *http.Request) {
	elem, err := strconv.Atoi(strings.Split(r.RequestURI, "/")[3])
	if err != nil {
		log.Fatalln(err)
	}
	if elem < len(users) {
		w.WriteHeader(http.StatusBadRequest)
	}

	copy(users[elem:], users[elem+1:])
	users[len(users)-1] = User{}
	users = users[:len(users)-1]

	w.WriteHeader(http.StatusOK)
}
