package services

func AddLetter(c chan string, combo string, alphabet string, len int) {
	if len <= 0 {
		return
	}
	var newCombo string
	for _, ch := range alphabet {
		newCombo = combo + string(ch)
		c <- newCombo
		AddLetter(c, combo, alphabet, len-1)
	}
}

func Gen(alphabet string, len int) <-chan string {
	c := make(chan string)
	go func(c chan string) {
		defer close(c)
		AddLetter(c, "", alphabet, len)
	}(c)
	return c
}
