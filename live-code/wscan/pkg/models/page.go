package models

type Item struct {
	Title string
	Desc  string
}
