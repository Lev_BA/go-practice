package main

import (
	"flag"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"log"
	"net/http"
	"sync"
	"wscan/pkg/models"
	"wscan/pkg/services"
)

func main() {
	alphabet := flag.String("alphabet", "", "Example abcdef")
	urlLen := flag.String("len", "", "Example 2")
	if *alphabet == "" && *urlLen == "" {
		flag.PrintDefaults()
		return
	}
	list := map[string]models.Item{}
	wg := sync.WaitGroup{}
	for domenName := range services.Gen("abcde", 3) {
		wg.Add(1)
		go func(domen string, wg sync.WaitGroup) {
			defer wg.Done()
			url := fmt.Sprintf("https://%s.ru", domenName)
			res, err := http.Get(url)
			if err != nil {
				fmt.Printf("Err:[%s]\n", err.Error())
				return
			}
			defer res.Body.Close()
			if res.StatusCode != 200 {
				log.Fatalf("Status code error: %d %s", res.StatusCode, res.Status)
				return
			}
			doc, err := goquery.NewDocumentFromReader(res.Body)
			if err != nil {
				fmt.Printf("Err:[%s]\n", err.Error())
				return
			}
			item := models.Item{}
			doc.Find("title").Each(func(i int, s *goquery.Selection) {
				item.Title = s.Text()
			})
			doc.Find("meta").Each(func(i int, s *goquery.Selection) {
				if s.AttrOr("name", "") == "description" {
					item.Desc = s.AttrOr("content", "")
				}
			})
			list[domenName] = item
		}(domenName, wg)
		wg.Wait()
	}
	println(len(list))
	for key, value := range list {
		fmt.Printf("Domen:[%s]\nTitle:[%s]\nDescription:[%s]", key, value.Title, value.Desc)
	}
}
