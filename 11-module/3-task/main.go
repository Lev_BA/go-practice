package main

import (
	"encoding/json"
	"fmt"
	"github.com/hashicorp/consul/api"
	_ "github.com/lib/pq"
)

/*type (
	envConfigV1 struct {
		Port string
		DSN  string
	}

	envConfigV2 struct {
		Port string

		DB struct {
			DSN string
		}

		Kafka struct {
			Brokers []string
		}
	}

	jsonConfig struct {
		App struct {
			Port string `json:"port"`
		} `json:"app"`
		DB struct {
			DSN string `json:"dsn"`
		} `json:"db"`
	}
)*/

/*type Config struct {
	App struct {
		Port string `json:"port"`
	} `json:"app"`

	Database struct {
		Dialect string `json:"dialect"`
		DSN     string `json:"dsn"`
	} `json:"database"`
}*/

type Config struct {
	Port string `json:"port"`
}

func main() {
	client, err := api.NewClient(api.DefaultConfig())
	if err != nil {
		panic(err)
	}

	kv := client.KV()

	p := &api.KVPair{Key: "APP_CONFIG", Value: []byte(`{"port": "3000" }`)}
	_, err = kv.Put(p, nil)
	if err != nil {
		panic(err)
	}

	pair, _, err := kv.Get("APP_CONFIG", nil)
	if err != nil {
		panic(err)
	}

	var cfg Config
	if err := json.Unmarshal([]byte(pair.Value), &cfg); err != nil {
		panic(err)
	}
	fmt.Printf("App port: %s", cfg.Port)
	/*jsonFile, err := os.Open("config.json")
	if err != nil {
		fmt.Println(err)
	}
	defer jsonFile.Close()
	bytes, err := io.ReadAll(jsonFile)
	if err != nil {
		fmt.Println(err)
	}

	var config Config
	if err := json.Unmarshal(bytes, &config); err != nil {
		panic(err)
	}

	//fmt.Println(os.Getenv("DB_DIALECT"), os.Getenv("APP_DB_DSN"))

	db, err := sql.Open(os.Getenv("DB_DIALECT"), os.Getenv("APP_DB_DSN"))
	if err != nil {
		panic(err)
	}
	defer db.Close()

	fmt.Println("Hello world!")

	_ = http.ListenAndServe(fmt.Sprintf(":%s", config.App.Port), nil)*/
}

/*var cfg envConfigV1

if err := envconfig.Process("APP", &cfg); err != nil {
	panic(err)
}

fmt.Println("PORT", cfg.Port)
fmt.Println("DSN", cfg.DSN)*/

/*mux := mux.NewRouters()

vp := viper.New()

vp.SetConfigFile("config")
vp.SetConfigType("json")
vp.AddConfigPath("./config/")
if err := vp.ReadInConfig(); err != nil {
	fmt.Println(err)
}

fmt.Println(vp.GetString("app"))*/
