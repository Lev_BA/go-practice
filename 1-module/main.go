package main

import (
	"fmt"
	"time"
)

func main() {
	nameStart := "Запуск программы!\n"

	fmt.Println(nameStart)

	getTime()
}

func getTime() {
	t := time.Now()

	day := t.Day()
	month := t.Month()
	year := t.Year()
	hour := t.Hour()
	minute := t.Minute()

	fmt.Printf("%d.%d.%d %d:%d", day, month, year, hour, minute)
}

//вопрос на счет даты, через какой метод задать дату через точки,
//избегая стандартных форм?