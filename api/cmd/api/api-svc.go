package main

import (
	"api-test/pkg/utils"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"
)

var authData = auth{
	User:     "user",
	Password: "user123",
}

type auth struct {
	User     string
	Password string
}

type Response struct {
	Status   int
	Services string
}

type User struct {
	Name string
	Age  int
}

func main() {
	port := utils.GetEnv("PORT", "9999")

	mux := http.NewServeMux()
	mux.HandleFunc("/api/v1", health)
	mux.HandleFunc("/api/v1/protected", basicAuth(protected))
	s := http.Server{
		Addr:         fmt.Sprintf(":%s", port),
		Handler:      mux,
		IdleTimeout:  time.Minute,
		ReadTimeout:  time.Second * 10,
		WriteTimeout: time.Second * 10,
	}
	log.Printf("Starting server on %s", s.Addr)
	if err := s.ListenAndServe(); err != nil {
		log.Fatalf("Err %v", err.Error())
	}
}

func basicAuth(handler http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		user, password, ok := r.BasicAuth()
		if !ok {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		if authData.User != user || authData.Password != password {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		orderID, ok := r.URL.Query()["id"]
		if !ok {
			w.WriteHeader(http.StatusBadRequest)
		}
		fmt.Println(orderID)
		userResp := User{Name: "Ivan", Age: 55}
		jsonData, err := json.Marshal(userResp)
		if err != nil {
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(jsonData)
		handler.ServeHTTP(w, r)
		fmt.Println(http.StatusOK)
	}
}

func health(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	dataBytes, _ := json.Marshal(Response{Status: http.StatusOK, Services: "api"})
	w.Write(dataBytes)
}

func protected(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	dataBytes, _ := json.Marshal(Response{Status: http.StatusOK, Services: "protected"})
	w.Write(dataBytes)
}
