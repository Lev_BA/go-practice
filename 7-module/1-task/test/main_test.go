package test

import (
	"testing"
)

type UserMap struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
}

func NewUserMap(name string, age int) *UserMap {
	return &UserMap{
		Name: name,
		Age:  age,
	}
}

func TestMapToStruct(t *testing.T) {

}

func TestStructToMap(t *testing.T) {
	u1 := NewUserMap("Lev", 29)

	m := StructToMap(u1)
}
