package main

import (
	"fmt"
	"reflect"
)

type User struct {
	Name   string `json:"name"`
	Age    uint   `json:"age"`
	Gender string
	Date   string
}

type UserMap struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
}

func NewUserMap(name string, age int) *UserMap {
	return &UserMap{
		Name: name,
		Age:  age,
	}
}

func main() {
	var u1 User
	map1 := StructToMap(u1)
	fmt.Println(u1, map1)

	u2 := NewUserMap("Alex", 29)

	m1 := map[string]interface{}{
		"name": "Andrey",
		"age":  54,
	}

	if err := MapToStruct(m1, u2); err != nil {
		fmt.Println(err)
	}

	fmt.Println(u2)
}

func MapToStruct(mp map[string]interface{}, item interface{}) error {
	m := reflect.ValueOf(mp)
	st := reflect.TypeOf(item).Elem()
	sv := reflect.ValueOf(item)

	for _, k := range m.MapKeys() {
		v := m.MapIndex(k)

		for i := 0; i < st.NumField(); i++ {
			vs := st.Field(i).Tag.Get("json")
			typeEl := sv.Elem().Field(i).Kind()
			valueStr := k.Interface().(string)

			if vs == valueStr && typeEl == reflect.String {
				sv.Elem().Field(i).SetString(v.Interface().(string))
			}
			if vs == valueStr && typeEl == reflect.Int {
				d := int64(v.Interface().(int))
				sv.Elem().Field(i).SetInt(d)
			}
		}
	}

	return nil
}

func StructToMap(item interface{}) map[string]interface{} {
	reflectValue := reflect.TypeOf(item)
	mapStruct := make(map[string]interface{})
	for i := 0; i < reflectValue.NumField(); i++ {
		field := reflectValue.Field(i)
		mapStruct[field.Name] = field.Type
	}

	fmt.Printf("Структура содержит: %d элемента:\n", reflectValue.NumField())
	return mapStruct
}

//Получение значения структуры
//.Elem().Field(0)
//sv1 := reflect.ValueOf(item).Elem().Field(1)

//Текстовое значение
//v0 := "Boris"
//sv0.SetString(v0)
//fmt.Println(k, m.MapIndex(k))
//получение значения тега
//fmt.Println(st.Field(0).Tag.Get("json"))
//Числовое значение
//var v1 int64
//v1 = 33
//sv1.SetInt(v1)
//fmt.Println(m, st, sv0, sv1)
/*switch vs == k.Interface().(string) {
case sv.Elem().Field(i).Kind() == reflect.String:


case sv.Elem().Field(i).Kind() == reflect.Int:
	fmt.Println("Числовая строка")
	//reflect.ValueOf(item).Elem().Field(i).SetString(v.(string))

}*/
/*if vs.Tag.Get("json") == k && st.Kind() == reflect.Int {

}*/
//fmt.Println(m, st)
/*reflectValue := reflect.TypeOf(item)
fmt.Println(reflectValue)
for k, v := range mp {
	fmt.Println(k, v)
	for i := 0; i < reflectValue.NumField(); i++ {
		field := reflectValue.Field(i)
		fmt.Println(field.Type)
	}
val := t.FieldByName(k)
	fmt.Println(val)
	val.Set(reflect.ValueOf(v))
}*/
/*val := reflect.ValueOf(mp)
fmt.Println("VALUE = ", val)
fmt.Println("KIND = ", val.Kind())
if val.Kind() == reflect.Map {
	for _, e := range val.MapKeys() {
		v := val.MapIndex(e)
		fmt.Println(e)
		fmt.Println(v)

	}
}*/
//val := reflect.MakeMap()
//fmt.Println(val)
/*func stringAssertion(v interface{}) (string, error) {
	reflectValue := reflect.ValueOf(v)
	if reflectValue.Kind() == reflect.String {
		return reflectValue.String(), nil
	}
	return "", errors.New("value is not string")
}*/
//var x float64 = 2.9
//v := reflect.ValueOf(&x).Elem()
//v.SetFloat(0.8)
//fmt.Println(x)

//user := User{"ivan", 25}
//reflectTypeUser := reflect.TypeOf(user)
//field := reflectTypeUser.Field(1)
//fmt.Println(field.Name, field.Type.Name(), field.Tag)
