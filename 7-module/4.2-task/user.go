package __2_task

//go:generate repogen

//go:repogen
type Config struct {
	ID    uint `gorm:"primary_key"`
	Login string
	Pass  string
}
