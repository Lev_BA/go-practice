package main

import (
	"bytes"
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"strings"
	"text/template"
)

var repositoryTemplate = template.Must(template.New("").Parse(`
	package main

	func StructToMap() map[string] interface{} {
		arrMap := make(map[string]interface{}, 0)
		arrMap["login"] = "{{ .Login }}"
		arrMap["Password"] = "{{ .Pass }}"
		
		return arrMap
	}
`))

type repositoryGenerator struct {
	typeSpec   *ast.TypeSpec
	structType *ast.StructType
}

//Напишем код для генерации самого репозитория
func (r repositoryGenerator) Generator(outFile *ast.File) error {
	//найдем строку первичного ключа и создадим метод, которые его нам вернет
	primary, err := r.primaryField()
	if err != nil {
		return err
	}

	//сформируем параметр для нашего шаблона
	params := struct {
		Login string
		Pass  string
	}{
		Login: primary.Names[0].Name,
		Pass:  "11321",
	}

	//вставим наши данные в шаблон
	var buf bytes.Buffer
	err = repositoryTemplate.Execute(&buf, params)
	if err != nil {
		return fmt.Errorf("execute template: %v", err)
	}

	//Далее преобразуем шаблон в синтаксическое дерево
	//также распарсим файл, но в содержимое передадим не путь к файлу
	//а содержимое буфера
	templateAst, err := parser.ParseFile(token.NewFileSet(), "", buf.Bytes(), parser.ParseComments)
	//Если распарсить не удалось, выведем ошибку
	if err != nil {
		return fmt.Errorf("parser file: %v", err)
	}
	//теперь нужно из синтаксического дерева сгенерированного файла скопировать все
	//общие декларации в результирующий файл

	for _, decl := range templateAst.Decls {
		outFile.Decls = append(outFile.Decls, decl)
	}

	return nil
}

//метод для провер первичного ключа
func (r repositoryGenerator) primaryField() (*ast.Field, error) {
	//перебор структуры для поискаключевого поля
	for _, field := range r.structType.Fields.List {
		//найдем если поле содержит слово primary
		if !strings.Contains(field.Tag.Value, "primary") {
			continue
		}

		return field, nil
	}
	//если поле не будет найдено, то возвращаем ошибку
	return nil, fmt.Errorf("has no primary field")
}
