package main

import (
	"fmt"
)

func main() {
	data := AnalysData("cmd/converter.go")
	fmt.Printf("Количество объявлений: %d\nКоличество присваиваний: %d\nКоличество ипортов: %d\nКоличество объявленных вызовов: %d", data.DeclCount, data.AssignCount,
		data.ImportsCount, data.CallCount)
}
