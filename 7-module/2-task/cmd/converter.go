package main

import (
	"fmt"
	"reflect"
)

type User struct {
	Name   string `json:"name"`
	Age    uint   `json:"age"`
	Gender string
	Date   string
}

type UserMap struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
}

func NewUserMap(name string, age int) *UserMap {
	return &UserMap{
		Name: name,
		Age:  age,
	}
}

func main() {
	var u1 User
	map1 := StructToMap(u1)
	fmt.Println(map1)

	u2 := NewUserMap("Alex", 29)

	m1 := map[string]interface{}{
		"name": "Andrey",
		"age":  54,
	}

	if err := MapToStruct(m1, u2); err != nil {
		fmt.Println(err)
	}

	fmt.Println(u2)
}

func MapToStruct(mp map[string]interface{}, item interface{}) error {
	m := reflect.ValueOf(mp)
	st := reflect.TypeOf(item).Elem()
	sv := reflect.ValueOf(item)

	for _, k := range m.MapKeys() {
		v := m.MapIndex(k)

		for i := 0; i < st.NumField(); i++ {
			vs := st.Field(i).Tag.Get("json")
			typeEl := sv.Elem().Field(i).Kind()
			valueStr := k.Interface().(string)

			if vs == valueStr && typeEl == reflect.String {
				sv.Elem().Field(i).SetString(v.Interface().(string))
			}
			if vs == valueStr && typeEl == reflect.Int {
				d := int64(v.Interface().(int))
				sv.Elem().Field(i).SetInt(d)
			}
		}
	}

	return nil
}

func StructToMap(item interface{}) map[string]interface{} {
	reflectValue := reflect.TypeOf(item)
	mapStruct := make(map[string]interface{})
	for i := 0; i < reflectValue.NumField(); i++ {
		field := reflectValue.Field(i)
		mapStruct[field.Name] = field.Type
	}

	fmt.Printf("Структура содержит: %d элемента:\n", reflectValue.NumField())
	return mapStruct
}
