package main

import (
	"7mod2task/models"
	"go/ast"
	"go/parser"
	"go/token"
)

func AnalysData(path string) *models.AnalysResult {
	var arrCall, arrDecl, arrAssign, arrImports []int

	fileSet := token.NewFileSet()
	node, err := parser.ParseFile(fileSet, path, nil, parser.ParseComments)
	if err != nil {
		panic(err)
	}

	ast.Inspect(node, func(n ast.Node) bool {
		b, ok := n.(*ast.CallExpr)
		if ok {
			arrCall = append(arrCall, int(b.Pos()))
		}

		u, ok := n.(*ast.DeclStmt)
		if ok {
			arrDecl = append(arrDecl, int(u.Pos()))
		}

		c, ok := n.(*ast.AssignStmt)
		if ok {
			arrAssign = append(arrAssign, int(c.Pos()))
		}

		g, ok := n.(*ast.ImportSpec)
		if ok {
			arrImports = append(arrImports, int(g.Pos()))
		}

		return true
	})

	return &models.AnalysResult{
		DeclCount:    len(arrDecl),
		CallCount:    len(arrCall),
		AssignCount:  len(arrAssign),
		ImportsCount: len(arrImports),
	}
}
