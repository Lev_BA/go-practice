package models

//go:generate repogen

//repogen:entity
type User struct {
	ID           uint `gorm:"primary_key"`
	Email        string
	PasswordHash string
}

type AnalysResult struct {
	DeclCount    int
	CallCount    int
	AssignCount  int
	ImportsCount int
}
