package models

import "time"

type User struct {
	ID   uint `gorm:"primary_key"`
	Name string
	Age  int
}

type Config struct {
	ID       int
	Login    string
	Password string
	Time     time.Time
}
