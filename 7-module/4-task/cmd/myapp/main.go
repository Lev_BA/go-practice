package main

import (
	"fmt"
	"go/ast"
	"io/ioutil"
	"os"
	"text/template"
	"time"
)

/*type User struct {
	Name string
	Age  int
}

func NewUser(name string, age int) *User {
	return &User{
		Name: name,
		Age:  age,
	}
}

type Config struct {
	ConvUser User
}*/

/*type repositoryGenerator struct {
	typeSpec   *ast.TypeSpec
	structType *ast.StructType
}*/
//go:generate
func main() {

	//var u1 models.Config

	generate()
	time.Sleep(1 * time.Second)
	arr := StructToMap()
	fmt.Println(arr)

}

func generate() error {
	byteText, err := ioutil.ReadFile("./template/marshaller2.gotmpl")
	if err != nil {
		fmt.Println(err)
	}

	t, err := template.New("StructToMap").Parse(string(byteText))
	if err != nil {
		panic(err)
	}
	fmt.Println(t)
	f, err := os.OpenFile("./cmd/myapp/helpers.go", os.O_WRONLY, 0777)
	if err != nil {
		return err
	}
	defer f.Close()

	u3 := Config{"2", "Wr", "admin", "09.04.2022"}

	err = t.Execute(f, u3)
	if err != nil {
		return err
	}

	fmt.Println(ast.Print(t))

	return nil
}

//func (r repositoryGenerator) Generate(outFile *ast.File) error {
//primary, err := r.primaryField()
//if err != nil {
//	return err
//}

/*params := struct {
	OutfilePath string
	Name        string
	Age         int
}{
	OutfilePath: "config/config.yml",
	Name:        "Boris",
	Age:         30,
}

var buf bytes.Buffer
err := repositoryTemplate.Execute(&buf, params)
if err != nil {
	return fmt.Errorf("exeute template: %v", err)
}

templateAst, err := parser.ParseFile(
	token.NewFileSet(),
	//Источник для парсинга лежит не в файле,
	"",
	//а в буфере
	buf.Bytes(),
	//mode парсинга, нас интересуют в основном комментарии
	parser.ParseComments,
)
if err != nil {
	return fmt.Errorf("parse template: %v", err)
}
//Добавляем декларации из полученного дерева
//в результирующий outFile *ast.File,
//переданный нам аргументом
for _, decl := range templateAst.Decls {
	outFile.Decls = append(outFile.Decls, decl)
}
return nil*/
//}

/*func (r repositoryGenerator) primaryField() (*ast.Field, error) {
	for _, field := range r.structType.Fields.List {
		if !strings.Contains(field.Tag.Value, "primary") {
			continue
		}

		return field, nil
	}

	return nil, fmt.Errorf("has no primary field")
}*/

/*path := os.Getenv("GOFILE")
if path == "" {
	log.Fatal("GODILE env variable must be set")
}

astInFile, err := parser.ParseFile(token.NewFileSet(), path, nil, parser.ParseComments)
if err != nil {
	log.Fatalf("parse file: %v", err)
}

i := inspector.New([]*ast.File{astInFile})
iFilter := []ast.Node{
	&ast.GenDecl{},
}

var genTasks []repositoryGenerator

i.Nodes(iFilter, func(node ast.Node, push bool) (proceed bool) {
	genDecl := node.(*ast.GenDecl)
	if genDecl.Doc == nil {
		return false
	}

	typeSpec, ok := genDecl.Specs[0].(*ast.TypeSpec)
	if !ok {
		return false
	}

	structType, ok := typeSpec.Type.(*ast.StructType)
	if !ok {
		return false
	}

	for _, comment := range genDecl.Doc.List {
		switch comment.Text {
		case "//myapp:entity":
			genTasks = append(genTasks, repositoryGenerator{
				typeSpec:   typeSpec,
				structType: structType,
			})
		}

	}

	return false
})*/

/*path := os.Getenv("GOFILE")
if path == "" {
	log.Fatal("GODILE env variable must be set")
}

astInFile, err := parser.ParseFile(token.NewFileSet(), path, nil, parser.ParseComments)
if err != nil {
	log.Fatalf("parse file: %v", err)
}

i := inspector.New([]*ast.File{astInFile})
iFilter := []ast.Node{
	&ast.GenDecl{},
}

type repositoryGenerator struct {
	typeSpec   *ast.TypeSpec
	structType *ast.StructType
}

var genTasks []repositoryGenerator

i.Nodes(iFilter, func(node ast.Node, push bool) (proceed bool) {
	genDecl := node.(*ast.GenDecl)
	if genDecl.Doc == nil {
		return false
	}

	typeSpec, ok := genDecl.Specs[0].(*ast.TypeSpec)
	if !ok {
		return false
	}

	for _, comment := range genDecl.Doc.List {
		switch comment.Text {
		case "//myapp:entity":
			genTasks = append(genTasks, repositoryGenerator{
				typeSpec:   typeSpec,
				structType: structType,
			})
		}

	}

	return false
})*/

//buffer := new(bytes.Buffer)

/**/
