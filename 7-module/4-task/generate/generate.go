package generate

import (
	"7mod4task/models"
	"fmt"
	"io/ioutil"
	"os"
	"text/template"
)

//repogen:entity
func main() {
	rf, err := ioutil.ReadFile("template/marshaller.gotmpl")
	if err != nil {
		fmt.Println(err)
	}

	wf, err := os.OpenFile("./cmd/myapp/repository.go", os.O_WRONLY, 0777)
	if err != nil {
		fmt.Println(err)
	}
	defer wf.Close()

	t := template.New("Generate_Map_Config")
	d, err := t.Parse(string(rf))
	if err != nil {
		fmt.Println(err)
	}

	err = d.Execute(wf, models.Config{})
}
