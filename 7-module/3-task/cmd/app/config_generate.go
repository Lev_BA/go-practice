package main

import (
	"7mod3task/models"
	"fmt"
	"html/template"
	"log"
	"os"
)

func ConfigGenerate(tmpl, outfilePath string) error {
	u1 := models.User{
		Name: "Alex",
		Age:  29,
	}

	err := generate(tmpl, outfilePath, u1)
	if err != nil {
		fmt.Println(err)
	}

	return nil
}

func generate(tmpl, outfilePath string, fields interface{}) error {
	t := template.New("fields")

	f, err := os.OpenFile(outfilePath, os.O_WRONLY, 0777)
	if err != nil {
		fmt.Println(err)
	}
	defer f.Close()

	d, err := t.Parse(tmpl)
	err = d.Execute(f, fields)
	if err != nil {
		log.Println(err)
	}

	return nil
}
