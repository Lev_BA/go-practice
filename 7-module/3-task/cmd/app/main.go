package main

import (
	tmpl "7mod3task/templates"
	"fmt"
)

func main() {
	err := ConfigGenerate(tmpl.Text, "./config/config")
	if err != nil {
		fmt.Println(err)
	}
}

/*message, err := ioutil.ReadFile("tpl.gohtml")
if err != nil {
	panic(err)
}

t, err := template.New("UserPage").Parse(string(message))
if err != nil {
	panic(err)
}

p := UsersPage{
	Title: "Users location",
	Users: []User{
		{
			Username: "Florin",
			Locations: map[string]Location{
				"Home": {
					Street:  "GoLand",
					ZipCode: "2018.3",
				},
			},
		},
	},
}

err = t.Execute(os.Stdout, p)
if err != nil {
	panic(err)
}*/

/*var letter = `
Dear {{.Name}},
{{if .Attended}}
It was a pleasure to see you at the wedding.
{{- else}}
It is a shame you couldn't make it to the wedding.
{{- end}}
{{with .Gift -}}
Thank you for the lovely {{.}}.
{{end}}
Best wishes,
Josie
`*/

/*type (
	Location struct {
		Street  string
		ZipCode string
	}
	User struct {
		Username  string
		Locations map[string]Location
	}
	UsersPage struct {
		Title string
		Users []User
	}
)*/

/*type Recipient struct {
	Name     string
	Gift     string
	Attended bool
}*/

/*var recipients = []Recipient{
	{"Aunt Mildred", "bone china tea set", true},
	{"Uncle John", "moleskin pants", false},
	{"Cousin Rodney", "", false},
}

t, err := template.New("letter").Parse(letter)
if err != nil {
	panic(err)
}

for _, r := range recipients {
	err := t.Execute(os.Stdout, r)
	if err != nil {
		log.Println("executing template:", err)
	}
}*/
