package main

import (
	"7mod5task/pkg"
	"fmt"
	_ "github.com/hexdigest/gowrap"
)

var (
	base   = pkg.BasePc{}
	home   = pkg.HomePc{Cpu: 4, GraphicsCard: 1, Wrapper: base}
	server = pkg.ServerPc{Cpu: 16, Memory: 256, Wrapper: base}
)

func main() {
	fmt.Printf("Base [%f]", base.GetPrice())
	fmt.Printf("Home Cpu [%d] Price[%f]\n", home.Cpu, home.GetPrice())
	fmt.Printf("Server [%f]", server.GetPrice())
}

/*type Worker interface {
	Start()
}

type worker struct {
	name string
}

type workerWithTime struct {
	worker Worker
}

func newWorkerWithTime(worker Worker) Worker {
	return &workerWithTime{worker: worker}
}

func newWorker(name string) Worker {
	return &worker{name: name}
}

func (w *worker) Start() error {
	fmt.Printf("worker %s is start\n", w.name)
	return nil
}

func (wwt *workerWithTime) Start() error {
	start := time.Now()
	defer func() {
		fmt.Println("worker done: time -", time.Since(start))
	}()

	return wwt.worker.Start()
}

func main() {
	w := newWorkerWithTime(newWorker("simple worker"))
	w.Start()
}*/
