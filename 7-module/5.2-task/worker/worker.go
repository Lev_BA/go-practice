package worker

import (
	"fmt"
	"sync"
)

type Worker interface {
	Start(wg *sync.WaitGroup)
}

type worker struct {
	n int
}

func NewWorker(n int) Worker {
	return &worker{n: n}
}

func (w *worker) Start(wg *sync.WaitGroup) {
	defer wg.Done()

	res := fib(w.n)
	fmt.Printf("%v", res)
}

func fib(n int) int64 {
	var res, a, b int64
	a = 0
	b = 1

	for n > 1 {
		res = a + b

		a = b
		b = res

		n--
	}

	return res
}
