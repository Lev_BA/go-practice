package worker

import (
	"fmt"
	"sync"
	"time"
)

type workerWithTime struct {
	worker Worker
}

func NewWorkerWithTime(worker Worker) Worker {
	return &workerWithTime{worker: worker}
}

func (w *workerWithTime) Start(wg *sync.WaitGroup) {
	startTime := time.Now()
	defer func() {
		fmt.Println("execution time:", time.Since(startTime))
	}()

	w.worker.Start(wg)
}
