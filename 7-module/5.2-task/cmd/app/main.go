package main

import (
	"7mod5task/worker"
	"sync"
)

/*var (
	base   = worker.BasePc{}
	home   = worker.HomePc{Cpu: 4, GraphicsCard: 1, Wrapper: base}
	server = worker.ServerPc{Cpu: 16, Memory: 256, Wrapper: base}
)*/

func main() {
	var wg sync.WaitGroup

	for i := 1; i < 10; i++ {
		wrk := worker.NewWorkerWithTime(worker.NewWorker(i))

		wg.Add(1)
		go wrk.Start(&wg)
	}

	wg.Wait()
}

/*type Worker interface {
	Start()
}

type worker struct {
	name string
}

type workerWithTime struct {
	worker Worker
}

func newWorkerWithTime(worker Worker) Worker {
	return &workerWithTime{worker: worker}
}

func newWorker(name string) Worker {
	return &worker{name: name}
}

func (w *worker) Start() error {
	fmt.Printf("worker %s is start\n", w.name)
	return nil
}

func (wwt *workerWithTime) Start() error {
	start := time.Now()
	defer func() {
		fmt.Println("worker done: time -", time.Since(start))
	}()

	return wwt.worker.Start()
}

func main() {
	w := newWorkerWithTime(newWorker("simple worker"))
	w.Start()
}*/
