package user

type User struct {
	Id       string `json:"id" bson:"_id, omitempty"`
	Username string `json:"username" bson:"username"`
	Password string `json:"-" bson:"password"`
	Email    string `json:"email" bson:"email"`
}

type CreateUserDTO struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Email    string `json:"email"`
}
