package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"time"
)

type Trainer struct {
	Name string
	Age  int
	City string
}

func main() {

	/*client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		log.Fatal(err)
	}

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)

	err = client.Ping(context.TODO(), readpref.Primary())
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Connect to MongoDB!")*/

	ctx := context.TODO()
	opts := options.Client().ApplyURI("mongodb://localhost:27017")

	client, err := mongo.Connect(ctx, opts)
	if err != nil {
		panic(err)
	}

	defer client.Disconnect(ctx)

	fmt.Printf("%T\n", client)

	if err := client.Ping(ctx, readpref.Primary()); err != nil {
		panic(err)
	}

	dbNames, err := client.ListDatabaseNames(ctx, bson.M{})
	if err != nil {
		panic(err)
	}

	fmt.Println(dbNames)

	testDB := client.Database("test")
	fmt.Printf("%T\n", testDB)

	exampleCollection := testDB.Collection("example")
	defer exampleCollection.Drop(ctx)

	fmt.Printf("%T\n", exampleCollection)

	example := bson.D{
		{"someString", "Example String"},
		{"someInteger", 12},
		{"someStringSlice", []string{"Example 1", "Example 2", "Example 3"}},
	}

	r, err := exampleCollection.InsertOne(ctx, example)
	if err != nil {
		panic(err)
	}
	fmt.Println(r.InsertedID)

	examples := []interface{}{
		bson.D{
			{"someString", "Second Example String"},
			{"someInteger", 253},
			{"someStringSlice", []string{"Example 15", "Example 42", "Example 83", "Example 5"}},
		},
		bson.D{
			{"someString", "Second Example String"},
			{"someInteger", 54},
			{"someStringSlice", []string{"Example 21", "Example 53"}},
		},
	}

	rs, err := exampleCollection.InsertMany(ctx, examples)
	if err != nil {
		panic(err)
	}
	fmt.Println(rs.InsertedIDs)

	c := exampleCollection.FindOne(ctx, bson.M{"_id": r.InsertedID})

	var exampleResult bson.M
	c.Decode(exampleResult)

	fmt.Printf("\nItem with ID: %v contains the following:\n", exampleResult["_id"])
	fmt.Println("someSting", exampleResult["someString"])
	fmt.Println("someInteger", exampleResult["someInteger"])
	fmt.Println("someStingSlice", exampleResult["someStringSlice"])

	filter := bson.D{{"someInteger", bson.D{{"$lt", 60}}}}
	examplesGT50, err := exampleCollection.Find(ctx, filter)
	if err != nil {
		panic(err)
	}
	var examplesResult []bson.M
	if err = examplesGT50.All(ctx, &examplesResult); err != nil {
		panic(err)
	}

	for _, e := range examplesResult {
		fmt.Printf("\nItem with ID: %v contains the following:\n", e["_id"])
		fmt.Println("someString:", e["someString"])
		fmt.Println("someInteger:", e["someInteger"])
		fmt.Println("someStringSlice:", e["someStringSlice"])
	}

	rUpdt, err := exampleCollection.UpdateByID(ctx, r.InsertedID, bson.D{
		{"$set", bson.M{"someInteger": 201}},
	})

	if err != nil {
		panic(err)
	}

	fmt.Println("Number of items updated", rUpdt.ModifiedCount)

	c2 := exampleCollection.FindOne(ctx, bson.M{"_id": r.InsertedID})
	var exampleResult2 bson.M
	c2.Decode(exampleResult2)

	fmt.Printf("/n Item with ID: %v contains the following: /n", exampleResult2["_id"])
	fmt.Println("someString", exampleResult2["someSting"])
	fmt.Println("someInteger", exampleResult2["someInteger"])
	fmt.Println("someStringSlice", exampleResult2["someStingSlice"])

	rUpdt, err = exampleCollection.UpdateOne(ctx, bson.M{"_id": r.InsertedID}, bson.D{
		{"$set", bson.M{"someString": "The Update String"}},
	})
	if err != nil {
		panic(err)
	}
	fmt.Println("Number of items update:", rUpdt.ModifiedCount)

	c3 := exampleCollection.FindOne(ctx, bson.M{"_id": r.InsertedID})

	var exampleResult3 bson.M
	c3.Decode(exampleResult3)

	fmt.Printf("/n Item with ID: %v contains the following: /n", exampleResult3["_id"])
	fmt.Println("someString", exampleResult3["someSting"])
	fmt.Println("someInteger", exampleResult3["someInteger"])
	fmt.Println("someStringSlice", exampleResult3["someStingSlice"])

	rUpdt2, err := exampleCollection.UpdateMany(
		ctx, bson.D{{"someInteger", bson.D{{"$gt", 60}}}},
		bson.D{
			{"$set", bson.M{"someInteger": 60}},
		},
	)

	if err != nil {
		panic(err)
	}
	fmt.Println("Number of items update:", rUpdt2.ModifiedCount)

	examplesAll, err := exampleCollection.Find(ctx, bson.M{})
	if err != nil {
		panic(err)
	}
	var examplesResult2 []bson.M
	err = examplesAll.All(ctx, &examplesResult2)
	if err != nil {
		panic(err)
	}

	for _, e := range examplesResult2 {
		fmt.Printf("\nItem with ID: %v contains the following:\n", e["_id"])
		fmt.Println("someString:", e["someString"])
		fmt.Println("someInteger:", e["someInteger"])
		fmt.Println("someStringSlice:", e["someStringSlice"])
	}

	rDel, err := exampleCollection.DeleteOne(ctx, bson.M{"_id": r.InsertedID})
	if err != nil {
		panic(err)
	}
	fmt.Println("Number of items deleted:", rDel.DeletedCount)

	time.Sleep(20 * time.Second)

	/*	dbNames, err := client.ListDatabaseNames(ctx, bson.M{})
		if err != nil {
			panic(err)
		}

		fmt.Println(dbNames)*/

}

func NewClient(ctx context.Context, host, port, username, password, database, authDB string) (db *mongo.Database, err error) {
	var (
		mongoDBURL string
		isAuth     bool
	)

	if username == "" && password == "" {
		mongoDBURL = fmt.Sprintf("mongodb://%s:%s", host, port)
	} else {
		isAuth = true
		mongoDBURL = fmt.Sprintf("mongodb://%s:%s@%s:%s", username, password, host, port)
	}

	clientOptions := options.Client().ApplyURI(mongoDBURL)
	if isAuth {
		if authDB == "" {
			authDB = database
		}
		clientOptions.SetAuth(options.Credential{
			AuthSource: authDB,
			Username:   username,
			Password:   password,
		})
	}

	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		return nil, fmt.Errorf("failed to connect to mongodb %v", err)
	}

	if err = client.Ping(ctx, nil); err != nil {
		return nil, fmt.Errorf("failed to ping mongodb %v", err)
	}

	return client.Database(database), nil
}

/*var (
	colection *mongo.Collection

)

func init() {
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017/")
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	err = client.Ping(ctx, nil)
	if err != nil {
		log.Fatal(err)
	}

	collection = client.Database("tasker").Collection("tasks")
}*/

/*type User struct {
	Name      string `json:"name" bson:"name"`
	Age       int    `json:"age" bson:"age"`
	Documents struct {
		PassportNumber string `json:"passport_number" bson:"passprot_number"`
		INN            int    `json:"inn" bson:"inn"`
	}
	CreateAt int64 `json:"create_at" bson:"create_at"`
}

func main() {
	u := User{
		Name:     "Брюзгин Лев Андреевич",
		Age:      27,
		CreateAt: time.Now().Unix(),
	}

	u.Documents.PassportNumber = "11111111"
	u.Documents.INN = 11111111

	sess, err := mongo.Dial("mongo://192.0.107.27017/cata")
	if err != nil {
		panic(err)
	}

	err = Store(sess, u)
	if err != nil {
		fmt.Println(err)
	}

}

func Store(sess *mongo.Session, user User) error {
	err := sess.DB("").C("users").Insert(user)
	if err != nil {
		return err
	}
	return nil
}*/
