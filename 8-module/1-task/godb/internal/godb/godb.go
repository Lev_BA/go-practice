package godb

//migrate create -ext sql -dir migrations -seq add_update_time
//migrate -database "postgres://price-manager:Buh7s7S4Gyf6xA1qczFk@10.90.100.31:30001/price-manager?sslmode=disable&search_path=public" -source=file://migrations up
import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"time"
)

type Instance struct {
	Db *pgxpool.Pool
}

type User struct {
	Name     string
	Age      int
	IsVerify bool
}

func (i *Instance) Start() {
	fmt.Println("Project godb started!")
	//тут будет распологаться код для запросов в БД
	//i.addUser(context.Background(), "Oleg", 31, false)
	/*for x := 0; x < 100; x++ {
		newName := fmt.Sprintf("Name%d", x)
		newAge := rand.Intn(80)
		var randomBool bool
		randomBool = true
		if rand.Intn(2) == 0 {
			randomBool = false
		}
		i.addUser(context.Background(), newName, newAge, randomBool)
	}*/
	i.addLastName(context.Background())
	i.deleteNotLastName(context.Background())
	i.getAllUsers(context.Background())
	//i.updateUserAge(context.Background(), "Lev", 31)
	//i.getUserByName(context.Background(), "Lev")
	//i.CleanTable(context.Background())
}

func (i *Instance) addUser(ctx context.Context, name string, age int, isVerify bool) {
	commandTag, err := i.Db.Exec(ctx, "INSERT INTO users (crated_at, name, age, verify) VALUES ($1, $2, $3, $4)", time.Now(), name, age, isVerify)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(commandTag.String())
	fmt.Println(commandTag.RowsAffected())
}

func (i *Instance) CleanTable(ctx context.Context) {
	commandTag, err := i.Db.Exec(ctx, "DELETE FROM users")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(commandTag.String())
}

func (i *Instance) getAllUsers(ctx context.Context) {
	var users []User

	rows, err := i.Db.Query(ctx, "SELECT name, age, verify FROM users;")
	if err == pgx.ErrNoRows {
		fmt.Println("No rows")
		return
	} else if err != nil {
		fmt.Println(err)
		return
	}
	defer rows.Close()

	for rows.Next() {
		user := User{}
		rows.Scan(&user.Name, &user.Age, &user.IsVerify)
		users = append(users, user)
	}

	fmt.Println(users)
}

func (i *Instance) deleteNotLastName(ctx context.Context) {
	_, err := i.Db.Exec(ctx, "DELETE FROM users WHERE verify=false;")
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (i *Instance) addLastName(ctx context.Context) {
	_, err := i.Db.Exec(ctx, "UPDATE users SET name=name || $1 WHERE verify=true;", " LastName")
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (i *Instance) updateUserAge(ctx context.Context, name string, age int) {
	_, err := i.Db.Exec(ctx, "UPDATE users SET age=$1 WHERE name=$2;", age, name)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (i *Instance) getUserByName(ctx context.Context, name string) {
	user := &User{}

	err := i.Db.QueryRow(ctx, "SELECT name, age, verify FROM users WHERE name=$1 LIMIT 1;", name).Scan(&user.Name, &user.Age, &user.IsVerify)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("User by name: %v\n", user)
}
