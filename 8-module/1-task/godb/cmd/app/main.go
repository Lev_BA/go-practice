package main

import (
	"context"
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"github.com/pressly/goose"
	"godb/internal/godb"
	"godb/pkg/helpers/pg"
	"os"
)

func main() {
	cfg := &pg.Config{}
	cfg.Host = "localhost"
	cfg.Username = "postgres"
	cfg.Password = "1111"
	cfg.Port = "54320"
	cfg.DbName = "db_test"
	cfg.Timeout = 5

	poolConfig, err := pg.NewPoolConfig(cfg)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Pool config error: %v\n", err)
		os.Exit(1)
	}

	poolConfig.MaxConns = 5

	c, err := pg.NewConnection(poolConfig)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Connect to datavase faild: %v\n", err)
		os.Exit(1)
	}
	fmt.Println("Connection OK!")

	mdb, _ := sql.Open("postgres", poolConfig.ConnString())
	err = mdb.Ping()
	if err != nil {
		panic(err)
	}
	err = goose.Up(mdb, "/var")
	if err != nil {
		panic(err)
	}

	_, err = c.Exec(context.Background(), ";")
	if err != nil {
		fmt.Fprintf(os.Stderr, "Ping faild: %v\n", err)
		os.Exit(1)
	}
	fmt.Println("Ping OK!")

	ins := &godb.Instance{Db: c}
	ins.Start()

}

/*connStr := fmt.Sprintf("%s://%s:%s@%s:%s/%s?sslmode=disable&connect_timeout=%d",
	"postgres",
	url.QueryEscape("postgres"),
	url.QueryEscape("1111"),
	"localhost",
	"54320",
	"db_test",
	5,
)

ctx, _ := context.WithCancel(context.Background())

poolConfig, _ := pgxpool.ParseConfig(connStr)
poolConfig.MaxConns = 20

pool, err := pgxpool.ConnectConfig(ctx, poolConfig)
if err != nil {
	fmt.Fprintf(os.Stderr, "Connect todatabase failed: %v\n", err)
	os.Exit(1)
}
fmt.Println("Connection OK!")

for i := 0; i < 10; i++ {
	go func(count int) {
		_, err = pool.Exec(ctx, ";")
		if err != nil {
			fmt.Fprintf(os.Stderr, "Ping failed: %v\n", err)
			os.Exit(1)
		}
		fmt.Println(count, "Query OK!")
		fmt.Printf("Conections - Max: %d, Iddle: %d, Total: %d \n",
			pool.Stat().MaxConns(), pool.Stat().IdleConns(), pool.Stat().TotalConns())
	}(i)
}*/

/*conn, err := pgx.Connect(ctx, conStr)
if err != nil {
	fmt.Fprintf(os.Stderr, "Connect to database faild: %v\n", err)
	os.Exit(1)
}
fmt.Println("Connection OK!")*/

/*for i := 0; i < 5; i++ {
	err = conn.Ping(ctx)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Ping failed: %v\n", err)
		os.Exit(1)
	}
	fmt.Println(i, "Query OK!")
}

for i := 0; i < 5; i++ {
	go func(i int) {
		err = conn.Ping(ctx)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Ping failed: %v\n", err)
			os.Exit(1)
		}
		fmt.Println(i, "Query OK!")
	}(i)
}*/

/*err = conn.Ping(ctx)
if err != nil {
	fmt.Fprintf(os.Stderr, "Ping faild %v\n", err)
	os.Exit(1)
}
fmt.Println("Query OK!")*/

//conn.Close(ctx)
//select {}
