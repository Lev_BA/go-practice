package app

import (
	"8mod4task/models"
	"fmt"
	"gorm.io/gorm"
	"math/rand"
	"strconv"
)

func Start(db *gorm.DB) {
	for i := 0; i <= 30; i++ {
		name := fmt.Sprintf("User-%d", i)
		age := randomNumber()
		cards := userCards(2)

		create(name, age, cards, db)
	}

	//create(db)
	//selectAndUpdate(db)
}
func create(name string, age int, cards []models.Card, db *gorm.DB) {
	u := &models.User{
		Name:     name,
		Age:      age,
		IsVerify: true,
		Cards:    cards,
	}

	result := db.Create(u)
	if result.Error != nil {
		panic(result.Error)
	}

	fmt.Printf("User created with ID: %d", u.ID)
}

func selectAndUpdate(db *gorm.DB) {
	user := &models.User{}
	db.Where("name = ?", "Anton").First(&user)
	if user.ID > 0 {
		user.Name = "Lev"
		db.Save(user)
	}
}

func randomNumber() int {
	minAge := 18
	maxAge := 70
	return rand.Intn(maxAge-minAge) + minAge
}

func userCards(numCards int) []models.Card {
	var arr []models.Card

	typeCards := []string{"VISA", "MasterCard", "МИР"}

	for i := 0; i <= numCards; i++ {
		num := rand.Intn(2-0) + 0
		card := models.Card{
			Number: strconv.Itoa(randomNumberCard()),
			Type:   typeCards[num],
		}

		arr = append(arr, card)
	}

	return arr
}

func randomNumberCard() int {
	minNum := 100000000
	maxNum := 900000000

	return rand.Intn(maxNum-minNum) + minNum
}
