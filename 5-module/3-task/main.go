package main

import (
	"fmt"
	"sync"
)

type Customer struct {
	Balance int
	mu      sync.Mutex
}

func NewCustomer(balance int) *Customer {
	return &Customer{
		Balance: balance,
	}
}

func (c *Customer) Withdraw(amount int) {
	c.mu.Lock()
	defer c.mu.Unlock()
	c.Balance -= amount
}

func (c *Customer) GetBalance() int {
	return c.Balance
}

func (c *Customer) Deposit(amount int) {
	c.mu.Lock()
	defer c.mu.Unlock()
	c.Balance += amount
}

func main() {
	Ani := NewCustomer(300)
	Bany := NewCustomer(200)

	go func() {
		Ani.Deposit(300)
	}()

	go func() {
		Bany.Deposit(200)
	}()

	fmt.Println(Ani.GetBalance())
}

/*//описание интерфейса работы
type Task interface {
	Execute()
}

//pooд - структура, нам потребуется Мутекс, для гарантий атомарности изменений
//Канал входных задач
//Канал отмены, для завершения работы
//MaitGroup для контроля завершения работ

type Pool struct {
	mu    sync.Mutex
	size  int
	tasks chan Task
	kill  chan struct{}
	wg    sync.WaitGroup
}

//Скроем внутреннее устройство за конструктором, пользователь может влиять только на размер пула
func NewPool(size int) *Pool {
	pool := &Pool{
		// Канал задач буферизированный, чтобы основная программа
		//не блокировалась при постановке задач
		tasks: make(chan Task, 128),
		//канал kill удаляет лишние воркер
		kill: make(chan struct{}),
	}
	//Вызовем метод resize, чтобы установить соответствующий размер пула
	pool.Resize(size)
	return pool
}

//жизненный цикл воркера
func (p *Pool) worker() {
	defer p.wg.Done()
	for {
		select {
		//если есть задача, то ее надо обработать
		case task, ok := <-p.tasks:
			if !ok {
				return
			}
			task.Execute()
			//если принял сигнал умирать, выходим
		case <-p.kill:
			return
		}
	}
}

func (p *Pool) Close() {
	close(p.tasks)
}

func (p *Pool) Wait() {
	p.wg.Wait()
}

func (p *Pool) Exec(task Task) {
	p.tasks <- task
}

func (p *Pool) Resize(n int) {
	//Закрываем лок, чтобы избежать одновременного изменения состояния
	p.mu.Lock()
	defer p.mu.Unlock()
	for p.size < n {
		p.size++
		p.wg.Add(1)
		go p.worker()
	}
	for p.size > n {
		p.size--
		p.kill <- struct{}{}
	}
}

func main() {
	pool := NewPool(5)

	pool.Exec(ExampleTask("foo"))
	pool.Exec(ExampleTask("bar"))

	pool.Resize(3)

	pool.Resize(6)

	for i := 0; i < 20; i++ {
		pool.Exec(ExampleTask(fmt.Sprintf("additional_%d", i+1)))
	}

	pool.Close()

	pool.Wait()
}*/

/*type AccountAsync struct {
	balance     float64
	deltaChan   chan float64
	balanceChan chan float64
	errChan     chan error
}

func NewAccount(balance float64) (a *AccountAsync) {
	a = &AccountAsync{
		balance:     balance,
		deltaChan:   make(chan float64),
		balanceChan: make(chan float64),
		errChan:     make(chan error, 1),
	}
	go a.run()
	return
}

//Просто читаем из канала баланса
func (a *AccountAsync) Balance() float64 {
	return <-a.balanceChan
}

//записываем количество в канал изменений
func (a *AccountAsync) Deposit(amount float64) error {
	a.deltaChan <- amount
	return <-a.errChan
}

//функция нужна для сохранения семантики
func (a *AccountAsync) Withdraw(amount float64) error {
	a.deltaChan <- -amount
	return <-a.errChan
}

func (a *AccountAsync) applyDelta(amount float64) error {
	stateStr := "Кладем на счет"
	if amount < 0 {
		stateStr = "Снимаем"
	}
	fmt.Println(stateStr, amount)

	newBalance := a.balance + amount
	if newBalance < 0 {
		return errors.New("недостаточно средств")
	}
	a.balance = newBalance

	return nil
}

//Бескоенчный цикл обработчика счета
//теперь сколько бы горутины не производили операции над этим аккаунтом
//все они будут синхронизированны здесь, и блокировки уже не нужны
func (a *AccountAsync) run() {
	var delta float64
	for {
		select {
		//если поступили изменения
		case delta = <-a.deltaChan:
			//попробуем их применить
			a.errChan <- a.applyDelta(delta)
			//если кто-то запрашивает баланс
		case a.balanceChan <- a.balance:
			//не делаем ничего, т.к. мы уже отправили ответ
		}
	}
}

func main() {
	acc := NewAccount(20)

	//стартуем 10 горутин
	for i := 0; i < 10; i++ {
		go func() {
			for j := 0; j < 10; j++ {
				//иногда снимает деньги
				if j%2 == 1 {
					acc.Withdraw(50)
					continue
				}
				//иногда кладет
				acc.Deposit(50)
			}
		}()
	}
	fmt.Scanln()
	//теперь баланс всегда будет сходится в 0
	fmt.Println(acc.Balance())
}*/

/*var wg sync.WaitGroup

	wg.Add(1)
	go func() {
		defer wg.Done()

		fmt.Println("1st gorutine sleeping...")
		time.Sleep(100 * time.Millisecond)
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()

		fmt.Println("2nd gorutine sleeping...")
		time.Sleep(200 * time.Millisecond)
	}()

	wg.Wait()
	fmt.Println("All gorutines complete.")
}*/

/*type BankCell struct {
	DollarCell float64
	mu         sync.Mutex
}

func (b *BankCell) GetBalance() float64 {
	b.mu.Lock()
	defer b.mu.Unlock()
	return b.DollarCell
}

func (b *BankCell) SubBalance(value float64) {
	b.mu.Lock()
	defer b.mu.Unlock()
	if b.DollarCell-value >= 0 {
		time.Sleep(time.Millisecond * 1)
		b.DollarCell -= value
	}
}

func (b *BankCell) AddBalance(value float64) {
	b.mu.Lock()
	defer b.mu.Unlock()
	b.DollarCell += value
}

func main() {
	bc := &BankCell{
		DollarCell: 10.0,
	}

	go bc.SubBalance(7.0)
	bc.SubBalance(7.0)
	time.Sleep(time.Second)

	fmt.Println(bc.GetBalance())
}*/
