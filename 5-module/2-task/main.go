package main

import (
	"fmt"
	"runtime"
	"time"
)

func main() {
	runtime.GOMAXPROCS(1)
	go spinner(100 * time.Millisecond)
	go calcFib(32)
	go calcFib(40)

	time.Sleep(10 * time.Second)
}

func calcFib(num int) {
	time.Sleep(3 * time.Second)
	fibN := fib(num)
	fmt.Printf("\rFibonacci(%d) = %d\n", num, fibN)
	runtime.Gosched()
}

func spinner(delay time.Duration) {
	for {
		for _, r := range `-\|/` {
			fmt.Printf("\r%c", r)
			time.Sleep(delay)
		}
	}
}

func fib(x int) int {
	if x < 2 {
		return x
	}
	return fib(x-1) + fib(x-2)
}
