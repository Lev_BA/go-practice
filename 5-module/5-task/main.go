package main

import (
	"context"
	"fmt"
	"net/http"
	"time"
)

func main(){
ctx := context.Background()
ctx,cancel := context.WithCancel(ctx)

go func() {
	err := cancelRequest(ctx)
	if err != nil {
		cancel()
	}
}()
doRequest(ctx, "https://ya.ru")
}

func cancelRequest(ctx context.Context) error {
	time.Sleep(100 * time.Microsecond)
	return fmt.Errorf("fail request")
}

func doRequest(ctx context.Context, requestStr string) {
	req, _ := http.NewRequest(http.MethodGet, requestStr, nil)
	req = req.WithContext(ctx)

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		//panic(err)
	}

	select {
	case <-time.After(500 * time.Microsecond):
		fmt.Printf("responcse copmleted, status code=%d", res.StatusCode)
	case <- ctx.Done():
		fmt.Println("request too long")
	}
}


/*type Customer struct {
	Name    string
	Age     int
	Deposit int
}

func NewCustomer(name string, age int, deposit int) *Customer {
	return &Customer{
		Name:    name,
		Age:     age,
		Deposit: deposit,
	}
}

func a(c chan<- int) {
	for {
		c <- 10
		c <- 20
	}
}*/

/*func process(input <-chan int) (res int) {
	for r := range input {
		res += r
	}
	return
}*/

/////////////////////////////////////////

/*func boring(msg string) <-chan string {
	c := make(chan string)
	go func() {
		for i := 0; ; i++ {
			c <- fmt.Sprintf("%s %d", msg, i)
			time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)
		}
	}()
	return c
}

func fanIn(input1, input2 <-chan string) <-chan string {
	c := make(chan string)
	go func() {
		for {
			c <- <-input1
		}
	}()
	go func() {
		for {
			c <- <-input2
		}
	}()

	go func() {
		for {
			select {
			case s := <-input1:
				c <- s
			case s := <-input2:
				c <- s
			}
		}
	}()
	return c
}
*/

//////////////////////////////////////////////////

/*func square(c chan int) {
	fmt.Println("[square] reading")
	num := <-c
	c <- num * num
}

func cube(c chan int) {
	fmt.Println("[cube] reading")
	num := <-c
	c <- num * num * num
}*/

/*func main() {
	ctx, cancel := context.WithCancel(context.Background())
	go handleSignals(cancel)*/

	/*ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond*300)
	tick := time.NewTicker(time.Millisecond * 50)

	for {
		select {
		case t := <-tick.C:
			fmt.Println(t)
			cancel()
		case <-ctx.Done():
			fmt.Println("context deadline exceeded")
			return
		default:
			fmt.Println("waiting")
			time.Sleep(time.Millisecond * 20)
		}
	}*/
	/*semaphore := make(chan int, 3)
		done := make(chan struct{})
		i := 0

		go func() {
			for ; ; i++ {
				semaphore <- i
				time.Sleep(time.Millisecond * 100)
			}
		}()
		go func() {
			time.Sleep(time.Millisecond * 1000)
			done <- struct{}{}
		}()
		msg := 0

	L:
		for {
			select {
			case msg = <-semaphore:
				fmt.Println(msg)

			case <-done:
				fmt.Println("done")
				break L
			default:
				if msg >= 20 {
					break L
				}
				fmt.Println("waiting")
				time.Sleep(time.Millisecond * 200)
			}
		}
		fmt.Println("success")*/

	/*fmt.Println("[main] main() stated")

	squareChan := make(chan int)
	cubeChan := make(chan int)

	go square(squareChan)
	go cube(cubeChan)

	testNum := 3
	fmt.Println("[main] sent testNum to sqareChan")

	squareChan <- testNum

	fmt.Println("[main] resuming")
	fmt.Println("[main] sent testNum to cubeChan")

	cubeChan <- testNum

	fmt.Println("[main] resuming")
	fmt.Println("[main] reading from channels")

	squareVal, cubeVal := <-squareChan, <-cubeChan
	sum := squareVal + cubeVal

	fmt.Println("[main] sum of square and cube of", testNum, " is", sum)
	fmt.Println("[main] main() stopped")/*

	/*fmt.Println("main() started")
	c := make(chan int, 3)

	go squares(c)

	c <- 1
	c <- 2
	c <- 3

	fmt.Println("main() stopped")*/

	/*c := fanIn(boring("Jee"), boring("Ann"))
	for i := 0; i < 18; i++ {
		fmt.Println(<-c)
	}
	fmt.Println("You're both boring; I'm leaving.")*/

	/*ch := str("Привет!")

	for i := 0; i < 5; i++ {
		fmt.Println(<-ch, i)
	}
	fmt.Println("The end!")*/
	/*c := boring("boring!")
	for i := 0; i < 5; i++ {
		fmt.Printf("You say: %q\n", <-c)
	}

	fmt.Println("You're boring; I'm leaving.")*/
	/*stuff := make(chan int, 7)
	go func() {

		for i := 0; i < 19; i = i + 3 {
			stuff <- i
		}
		close(stuff)
	}()
	fmt.Println("Res", process(stuff))*/
	/*c := make(chan int)

	go a(c)
	for i := 0; i < 5; i++ {
		fmt.Println(<-c + <-c)
	}*/

	/*c := make(chan string)

	go greet(c)
	for i := 0; i < 5; i++ {
		fmt.Println(<-c, ", ", <-c)
	}*/
/*}

func greet(ch chan<- string) {
	for {
		ch <- fmt.Sprintf("Первое слово")
		ch <- fmt.Sprintf("Второе слово")

	}
}*/
