package main

import (
	"fmt"
	"time"
)

func main() {
	go spinner(100 * time.Millisecond)
	go calcFib(44)
	calcFib(45)

	time.Sleep(10 * time.Second)
}

func calcFib(num int) {
	fibN := fib(num)
	fmt.Printf("\rFibonacci(%d) = %d\n", num, fibN)
}

func spinner(delay time.Duration) {
	for {
		for _, r := range `-\|/` {
			fmt.Printf("\r%c", r)
			time.Sleep(delay)
		}
	}
}

func fib(x int) int {
	if x < 2 {
		return x
	}
	return fib(x-1) + fib(x-2)
}
