package main

import (
	"fmt"
	"sync"
	"time"
)

/*var (
	sema    = make(chan struct{}, 1) //бинарный семафор
	balance int
)

func Deposit(amount int) {
	sema <- struct{}{}
	balance = balance + amount
	<-sema
}

func Balance() int {
	sema <- struct{}{}
	b := balance
	<-sema
	return b
}*/

//не понятно, что должно получиться
const (
	k1   = "key1"
	step = 7
)

type Cache struct {
	storage map[string]int
	mu      sync.RWMutex
}

func NewCache(initStorage map[string]int) *Cache {
	if initStorage != nil {
		return &Cache{
			storage: initStorage,
		}
	}
	return &Cache{
		storage: make(map[string]int),
	}
}

func (m *Cache) GetValue(key string) int {
	m.mu.RLock()
	defer m.mu.RUnlock()
	return m.storage[key]
}

func (m *Cache) SetValue(key string, value int) {
	m.mu.Lock()
	defer m.mu.Unlock()
	m.storage[key] = value
}

func (m *Cache) IncreaseValue(key string, value int) {
	m.mu.Lock()
	defer m.mu.Unlock()
	m.storage[key] += value
}

func (m *Cache) GetKeys() []string {
	m.mu.RLock()
	defer m.mu.RUnlock()
	keys := make([]string, 0, len(m.storage))
	for k := range m.storage {
		keys = append(keys, k)
	}
	return keys
}

func (m *Cache) Print() {
	m.mu.RLock()
	defer m.mu.RUnlock()
	for k, v := range m.storage {
		fmt.Printf("%s:%v\n", k, v)
	}
}

func main() {

	b := make(map[string]int)

	cache := NewCache(b)

	var wg sync.WaitGroup

	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			cache.IncreaseValue(k1, step)
			time.Sleep(time.Millisecond * 100)
		}()
	}

	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			cache.SetValue(k1, step*i)
			time.Sleep(time.Millisecond * 100)
		}(i)
	}

	wg.Wait()

	fmt.Println(cache.GetValue(k1))
}
	/*m := NewStorage(map[string]float64{
		"Alex":  10.0,
		"Paul":  40.0,
		"Frank": 15.0,
	})

	m.Print()
	fmt.Println()

	var wg sync.WaitGroup

	for i := 0; i < 5; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for _, key := range m.GetKeys() {
				time.Sleep(time.Millisecond * 10)
				m.SetValue(key, m.GetValue(key)+1)
			}
		}()
	}

	time.Sleep(time.Second)
	fmt.Println()
	m.Print()*/

