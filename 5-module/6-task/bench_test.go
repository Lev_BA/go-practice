package main

import (
	"sync"
	"testing"
)

type Counter struct {
	A int
	B int
}

var pool = sync.Pool{
	New: func() interface{} {
		return new(Counter)
	},
}

func incA(s *Counter) { s.A++ }
func incB(s *Counter) { s.B++ }

///Показатель 145.894 с
func BenchmarkWithoutPool(b *testing.B) {
	var s *Counter
	for i := 0; i < b.N; i++ {
		for j := 0; j < 10000; j++ {
			s = &Counter{
				A: 1,
			}
			b.StopTimer()
			incA(s)
			b.StartTimer()
		}
	}
}

//Показатель 259.66 с
/*func BenchmarkWithPool(b *testing.B) {
	var s *Counter
	for i := 0; i < b.N; i++ {
		for j := 0; j < 10000; j++ {
			s = pool.Get().(*Counter)
			s.B = 1
			b.StopTimer()
			incB(s)
			b.StartTimer()
			pool.Put(s)
		}
	}
}*/

/*func BenchmarkSample(b *testing.B) {
	for i := 0; i < b.N; i++ {
		if x := fmt.Sprintf("%d", 42); x != "42" {
			b.Fatalf("Unexpected string: %s", x)
		}
	}
}*/
