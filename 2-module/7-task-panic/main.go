package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	//"io/ioutil"
)

func main() {

	pathIn := "E:/!GO/src/go-practice/2-module/7-task-panic/data/in.txt"
	pathOut := "E:/!GO/src/go-practice/2-module/7-task-panic/data/out.txt"

	//открытие файла с данными
	openFile, err := os.Open(pathIn)
	if err != nil {
		panic(err)
	}
	defer openFile.Close()

	//открытие файла куда нужно сделать запись и добавление записи в файл
	openFileRecord, err := os.OpenFile(pathOut, os.O_APPEND | os.O_WRONLY, 0644)
	defer openFileRecord.Close()
	if err != nil {
		panic(err)
	}

	//сканироание файла по строкам
	fileScanner := bufio.NewScanner(openFile)

	//перебор строк файла
	for fileScanner.Scan() {

		//Каждую строку разбиваем на части, разделитель является |
		textCategory := strings.Split(fileScanner.Text(), "|")

		//Разбиение частей на группы и конкантенация в общий информационный блок
		categoryItem := ("Name: " +  textCategory[0] + "\n" +
										"Adress: " + textCategory[1] + "\n" +
										"City: " + textCategory[2] + "\n")

		//если строка пустая, то вызавыется паника
		handleRow(textCategory[1])

		//превразение получившегося информационного блока в байтовый срез
		data := []byte(categoryItem)

		//запись данных в файл
		_, err = openFileRecord.Write(data)
		if err != nil {
			panic(err)
		}
	}
}

func handleRow(row string) {
	defer func() {
		if e := recover(); e != nil {
			fmt.Println(e)
		}
	}()

	if row == "" {
		panic("пустая строка")
	}
}



	/*for {
		readFile, _ := openFile.Read(data)
		if readFile == 0 {
			break
		}
	}

	textData := string(data)
	//124 байт в символе |
	fmt.Println(len(data), data[3], string(data[3]), textData)*/


	//перебор текстового промежутка по байтам и определение разделительных черт

	/*var sepNumber uint32
	for i := range data {
		if data[i] == 124 {
			sepNumber++
			fmt.Println("Разделительная черта")
		}
	}

	fmt.Println(sepNumber)*/

	//создаем файл
	/*newFile, err := os.Create("data/out.txt")
	if err != nil {
		fmt.Printf("Ошибка создания файла [%s]", err.Error())
		return
	}

	writeString, err := newFile.WriteString(text)
	if err != nil {
		fmt.Printf("Ошибка записи данных [%s]", err.Error())
		return
	}

	openFile, err := os.Open(newFile.Name())
	if err != nil {
		fmt.Printf("Ошибка чтения файла [%s]", err.Error())
		return
	}
	data := make([]byte, writeString)

	for {
		_, err := openFile.Read(data)
		if err != io.EOF {
			break
		}
		if err != nil {
			fmt.Printf("Ошибка чтения файла [%s]", err.Error())
		}
	}*/

