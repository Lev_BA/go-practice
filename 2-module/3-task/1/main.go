package main

import "fmt"

func main() {
	//создаем слайс с днями недели
	week := []string{"Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"}

	//делаем срез рабочих дней
	workingDay := week[:5]

	//делаем срез выходных дней
	week = week[5:]

	fmt.Println(workingDay, week)
}
