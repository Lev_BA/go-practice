package main

import "fmt"

func main() {
	week := []string{"Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"}
	workingDay := week[:5] //срез с рабочими днями
	week = week[5:]        //срез с выходными днями

	//содеиняем рабочие и выходные дни с помощью append
	newWeek := append(workingDay, week...)

	fmt.Println(newWeek)
}
