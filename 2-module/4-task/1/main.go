package main

import "fmt"

func main() {
	//отображение содержит данные о читателях, категориях книг и их названиях, выданные читателю
	readers := map[string]map[string][]string{
		"reader1": {
			"books":     {"Бронзовая птица", "Идиот", "Братья Карамазовы"},
			"magazines": {"Космополитен", "Максим"},
		},
		"reader2": {
			"books":     {"Язык программирования Go", "Кортик"},
			"magazines": {"7 дней", "Хакер"},
		},
	}

	//Проверка на существование читателя
	//reader, ok := readers["reader1"]

	//Вопрос как запустить проверку отдельной функцией с помощью аргумента?
	/*if !ok {
		fmt.Println("Данного читателя нет")

		return
	}*/

	fmt.Println(readers[])

	//getAllBooks(reader)
}

//функция получает читателя и возвращет количество и название всех книг взятые указанным читателем
func getAllBooks(person map[string][]string) {
	var arr []string

	for _, v := range person {
		arr = append(arr, v...)
	}

	fmt.Println(len(arr), arr)
}

/*category := "books"

if category == "books" {
	sum := 0

	for _, a := range readers {
		v := len(a["books"])
		sum += v
	}

	fmt.Println(sum)
}

if category == "magazines" {
	sum := 0

	for _, a := range readers {
		v := len(a["magazines"])
		sum += v
	}

	fmt.Println(sum)
}

if category == "" {
	fmt.Println("Введите значение в параметр")
}

readerNumBook := "reader1"

for _, a := range readers[readerNumBook] {
	fmt.Println(len(a))
}*/
