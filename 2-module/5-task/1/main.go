package main

import "fmt"

func main() {
	//срез вариации строк
	arr := []string{
		"one",
		"two",
		"three",
	}

	//Значение строки для сравнения
	value := "three"

	contains(arr, value)

	fmt.Printf("Максимальное значение: %d", getMax(3, 47, 5, 47, 7))
}

//функция принимает параметры - срез со значениями и строка для сравнения с элементами среза
//При совпадении с одним из элементов среза возвращает true
//иначе false
func contains(a []string, x string) bool {
	for _, value := range a {
		if value == x {
			fmt.Println("Значение совпадает")

			return true
		}
	}

	fmt.Println("Значение не найдено")

	return false
}

//функция принимает несколько параметров из целых чисел
//сравнивает их, если число меньше максимального, то начинает новую итерацию
//если больше, то перезаписывает максимальное число
func getMax(x ...int) int {
	var maxValue int

	for _, v := range x {
		if v < maxValue {
			continue
		}

		maxValue = v
	}

	return maxValue
}

//Надо объяснить как учавствуют значения в скобках после литералов
/*func(w ...string) {
	printWord(w...)
}("str1", "str2")*/
