// Задача получилась без использования defer

package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

func main() {
	//Сообщение которое записывается сначало в файл data/in.txt, а далее копируется в файл out.txt
	message := "Гагарин первый полетел в космос! Поехали!"

	//создали папку data
	newDirectory := os.Mkdir("data", 0777)

	//ввели переменную текста в байтовом типе
	var newText []byte

	if newDirectory != nil {
		newText = createFile("data/in.txt", message)
	}

	newFile := createFile("out.txt", string(newText))

	fmt.Println(string(newFile))
}

//функция получает на входе параметр пути и имени файла и текст этого файла, на выходе возвращает байтовый код
func createFile(nameFile string, text string) []byte {
	data := []byte(text)
	e := ioutil.WriteFile(nameFile, data, 0600)
	if e != nil {
		fmt.Println(e)
	}

	newFile, err := ioutil.ReadFile(nameFile)
	if err != nil {
		panic(err)
	}

	return newFile
}

//попытки сделать задачу

//text := createFile("data/in.txt")

//fmt.Println(text)

/*newFile, err := os.OpenFile("out.txt", os.O_WRONLY, 0666)
if err != nil {
	fmt.Println(err)
}
defer newFile.Close*/

//newFile.Write(text)
//fmt.Fprintln(os.Stdout, newFile)
