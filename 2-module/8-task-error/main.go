package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type UserError struct {
	Text string
	Err  error
}

func (e *UserError) Error() string {
	return fmt.Sprintf("ошибка: %s, %v", e.Text, e.Err)
}

func main() {
	pathIn := "E:/www/2-module/8-task-error/data/in.txt"
	total, err := countLines(pathIn)
	if err != nil {
		panic(err)
	}

	fmt.Printf("Total strings: %d", total)
}

//функция получает на вход путь считаемого файла и возвращает количество строк или ошибку
func countLines(path string) (total int, err error) {
	//открытие файла
	fileOpen, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer fileOpen.Close()

	fileScanner := bufio.NewScanner(fileOpen)
	//переменная для отслеживания номера строки
	var strNumber int
	for fileScanner.Scan() {
		strNumber++
		textCategory := strings.Split(fileScanner.Text(), ",")
		order, err := strconv.Atoi(textCategory[0])
		if err != nil {
			fmt.Println("Ошибка в строке " + strconv.Itoa(strNumber))
			return 0, err
		}
		//если номер строки не совпадет с номинальным номером,
		//то возвращает пользовательскую ошибку и номер строки
		if strNumber != order {
			err := &UserError{
				Text: "строка - " + strconv.Itoa(strNumber),
				Err:  errors.New("возникла ошибка в нумерации строки"),
			}
			return 0, err
		}
	}

	return strNumber, nil
}

//Не выполнено задание обработки окончания файла io.EOF
