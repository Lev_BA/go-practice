package main

import "fmt"

func main() {
	var a *int

	b := 43

	a = &b

	fmt.Println(*a)

	*a = 55

	fmt.Println(*a)
}
