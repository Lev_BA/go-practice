package main

import (
	"fmt"
	"math"
)

func main() {
	const pi float64 = math.Pi
	var (
		circleLength float64 = 35                      //длина окружности
		radius       float64 = circleLength / (2 * pi) //определение радиуса
	)

	//указатель на радиус
	r := &radius

	//определение площади окружности
	s := pi * math.Pow(*r, 2)

	fmt.Println(s)
}
