package main

import "fmt"

func main() {
	type AmericanVelocity float32
	type EuropeanVelocity float32

	var speedMeters float32 = 104.2
	var kmInMl float32 = 1.609

	speedKm := speedMeters * float32(3.6)
	speedMl := speedKm / kmInMl

	//Почему данный вариант не работает?
	//var speedKm EuropeanVelocity = speedMeters * float32(3.6)
	//var speedMl AmericanVelocity = speedKm / kmInMl

	fmt.Println(speedKm, speedMl)
}
