package main

import (
	"fmt"
	"log"
	"strconv"
)

func main() {
	firstNum := "104"
	lastNum := 35

	a, err := strconv.Atoi(firstNum)

	if err != nil {
		log.Fatal(err)
	}

	b := strconv.Itoa(lastNum)

	fmt.Println(a)
	fmt.Println(b)
}
