package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
)

//struct
type CustomError struct {
	Err error
}

type excessLimit struct {
	message    error
	limit      int
	lastString int
}

//method
func (e *CustomError) Error() string {
	//err := fmt.Errorf("new error: %s", e.Custom)
	return e.Err.Error()
}

func (e *excessLimit) Error() string {
	return fmt.Sprintf("%s, limit: %d, last string: %d", e.message, e.limit, e.lastString)
}

//function
func custErr(text string) error {
	return &CustomError{
		Err: fmt.Errorf(text),
	}
}

func customLimitErr(textErr string, lim int, lastStr int) error {
	return &excessLimit{
		message:    fmt.Errorf(textErr),
		limit:      lim,
		lastString: lastStr,
	}
}

const (
	LIMIT     = 150
	PATH_FILE = "E:/www/go-practice/4-module/6-task/data/in.txt"
)

func main() {
	fileOpen, err := os.Open(PATH_FILE)
	if err != nil {
		log.Println(err)
	}
	defer fileOpen.Close()

	l, err := CountLines(fileOpen, LIMIT)
	if err != nil {
		fmt.Println("string count exceed limit, please read another file err: ", err)
	}

	fmt.Println(l)
}

func CountLines(r io.Reader, limit int) (int, error) {
	var (
		br    = bufio.NewReader(r)
		lines int
		err   error
	)

	for {
		_, err = br.ReadString('\n')
		lines++
		if err != nil {
			break
		}
	}

	if lines > limit {
		e := customLimitErr("limit err", limit, lines)
		return 0, fmt.Errorf(e.Error())
	}

	if err != io.EOF {
		e := custErr("EOF error")
		return 0, fmt.Errorf(e.Error())
	}

	return lines, nil
}

//Версия модуль 2, таск 8

/*type UserError struct {
	Text string
	Err  error
}

func (e *UserError) Error() string {
	return fmt.Sprintf("ошибка: %s, %v", e.Text, e.Err)
}

func main() {
	pathIn := "E:/www/go-practice/2-module/8-task-error/data/in.txt"
	total, err := countLines(pathIn)
	if err != nil {
		panic(err)
	}

	fmt.Printf("Total strings: %d", total)
}

//функция получает на вход путь считаемого файла и возвращает количество строк или ошибку
func countLines(path string) (total int, err error) {
	//открытие файла
	fileOpen, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer fileOpen.Close()

	fileScanner := bufio.NewScanner(fileOpen)
	//переменная для отслеживания номера строки
	var strNumber int
	for fileScanner.Scan() {
		strNumber++
		textCategory := strings.Split(fileScanner.Text(), ",")
		order, err := strconv.Atoi(textCategory[0])
		if err != nil {
			fmt.Println("Ошибка в строке " + strconv.Itoa(strNumber))
			return 0, err
		}
		//если номер строки не совпадет с номинальным номером,
		//то возвращает пользовательскую ошибку и номер строки
		if strNumber != order {
			err := &UserError{
				Text: "строка - " + strconv.Itoa(strNumber),
				Err:  errors.New("возникла ошибка в нумерации строки"),
			}
			return 0, err
		}
	}

	return strNumber, nil
}*/
