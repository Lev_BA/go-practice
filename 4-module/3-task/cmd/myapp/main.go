package main

import (
	"errors"
	"fmt"
	"task/internal"
)

//const PRICE_PRODUCT = 700

func main() {
	cust := internal.NewCustomer("Tolya", 23, 13000, 1000, false)
	partner := internal.NewPartner("Alexander", 43, 20000, 10000)

	s := "Текст"

	fmt.Println(startTransactionDinamic(s))
	fmt.Println(startTransactionDinamic(partner))
	fmt.Println(partner)

	totalPrice, err := internal.CalcPrice(cust, 1500)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("Итоговая цена продукта: %v", totalPrice)
}

func startTransaction(debtor internal.Debtor) error {
	return debtor.WrOffDebt()
}

func startTransactionDinamic(d interface{}) error {
	debtor, ok := d.(internal.Debtor)
	if !ok {
		return errors.New("incorrect type")
	}
	err := debtor.WrOffDebt()

	if err != nil {
		return err
	}

	return nil
}
