package main

import (
	"fmt"
	"task/internal"
)

const PRICE_PRODUCT = 700

func main() {
	cust := internal.NewCustomer("Boris", 18, 19000, 600, true)
	cust.WrOffDebt()

	cust2 := internal.NewCustomer("Alex", 120, 20000, 1000, false)
	cust2.WrOffDebt()

	fmt.Printf("%+v\n", cust)
	fmt.Printf("%+v\n", cust2)
	
	c, err := cust2.CalcDiscount()
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(c)

	/*totalPrice, err := internal.CalcPrice(cust, PRICE_PRODUCT)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Printf("Итоговая цена продукта с учетом скидки: %d", totalPrice)*/
}
