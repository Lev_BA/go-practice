package internal

import "time"

type Card struct {
	Balance     int
	ExpiredDate string
	CVV         int
	Num         int64
	Owner       string
}

type CreditCard struct {
	Card
	Limit int
}

func (c *Card) CheckDate() bool {
	ex, _ := time.Parse("2006.01.02", c.ExpiredDate)
	return time.Now().Before(ex)
}
