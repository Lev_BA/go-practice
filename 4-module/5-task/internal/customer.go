package internal

import "errors"

const DEFAULT_DISCOUNT = 500

type Partner struct {
	Name    string
	Age     int
	balance int
	debt    int
}

type Customer struct {
	Name     string
	Age      int
	discount bool
	overduer Overduer
}

type Overduer struct {
	balance int
	debt    int
}

func (c *Customer) CalcDiscount() (int, error) {
	if c.discount {
		return 0, errors.New("discount not available")
	}
	result := DEFAULT_DISCOUNT - c.overduer.debt
	if result < 0 {
		return 0, nil
	}
	return result, nil
}

func (c *Customer) WrOffDebt() error {

	if c.overduer.balance >= c.overduer.debt {
		return errors.New("not possible write off")
	}

	c.overduer.balance -= c.overduer.debt
	c.overduer.debt = 0

	return nil
}

func (c *Partner) WrOffDebt() error {
	c.debt = 0

	return nil
}

func NewCustomer(name string, age int, balance int, debt int, discount bool) *Customer {
	overduer := Overduer{balance: balance, debt: debt}

	return &Customer{
		Name:     name,
		Age:      age,
		discount: discount,
		overduer: overduer,
	}
}

func NewPartner(name string, age int, balance int, debt int) *Partner {
	return &Partner{
		Name:    name,
		Age:     age,
		balance: balance,
		debt:    debt,
	}
}

func CalcPrice(c Discounter, price int) (int, error) {
	disc, err := c.CalcDiscount()
	if err != nil {
		return 0, errors.New("ошибка в расчете скидки")
	}
	price -= disc

	return price, nil
}
