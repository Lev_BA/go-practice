package internal

type Debtor interface {
	WrOffDebt() error
}

type Discounter interface {
	CalcDiscount() (int, error)
}
