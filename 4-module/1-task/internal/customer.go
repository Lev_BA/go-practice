package internal

import "errors"

type Customer struct {
	Name         string
	Age          int
	Balance      int
	Debt         int
	Discount     bool
	CalcDiscount func() (int, error)
}

func NewCustomer(name string, age int, balance int, debt int, discount bool) *Customer {
	return &Customer{
		Name:     name,
		Age:      age,
		Balance:  balance,
		Debt:     debt,
		Discount: discount,
	}
}

func CalcPrice(c *Customer, price int) (int, error) {
	disc, err := c.CalcDiscount()
	if err != nil {
		return 0, errors.New("ошибка в расчете скидки")
	}
	price -= disc

	return price, nil
}
