package main

import (
	"errors"
	"fmt"
	"task/internal"
)

const (
	DEFAULT_DISCOUNT = 500
	PRICE_PRODUCT    = 700
)

func main() {
	cust := internal.NewCustomer("Boris", 18, 19000, 600, true)

	cust.CalcDiscount = func() (int, error) {
		if !cust.Discount {
			return 0, errors.New("Discount not available")
		}
		result := DEFAULT_DISCOUNT - cust.Debt
		if result < 0 {
			return 0, nil
		}
		return result, nil
	}

	totalPrice, err := internal.CalcPrice(cust, PRICE_PRODUCT)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Printf("Итоговая цена продукта с учетом скидки: %d", totalPrice)
}
