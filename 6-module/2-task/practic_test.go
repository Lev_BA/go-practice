package main

import (
	mock_main "2-task"
	"github.com/golang/mock/gomock"
	_ "github.com/stretchr/testify/require"
	"testing"
)

type fakeTransport struct {
	Ticket *Ticket
	Err    error
}

type fakePass struct {
	Name string
	Age  int
}

func TestRegistrationTicket(t *testing.T) {
	t.Run("registration", func(t *testing.T) {
		//req := require.New(t)
		any := gomock.Any()

		mockCtrl := gomock.NewController(t)
		defer mockCtrl.Finish()

		storageMock := mock_main.NewMockOptions(mockCtrl)

		storageMock.EXPECT().RegistrationTicket(any).Return(nil, nil)

		//fakeNewTransport := fakeTransport{Ticket: nil, Err: nil}
		//fakeNewPass := fakePass{Name: "Test", Age: 30}

		//order, err := fakeNewTransport.RegistrationTicket(fakeNewPass)

	})
}

//Mocks

func (ft *fakeTransport) RegistrationTicket(pass fakePass) (*Ticket, error) {
	return ft.Ticket, ft.Err
}
