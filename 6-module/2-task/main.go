package main

/*type User struct {
	SessionID string
	Name      string
	Pass      string
}

type Session struct {
	ID     string
	UserID int
}

type Storage interface {
	RecoverSession(id string) (*Session, error)
	CreateSession(name string, pass string) (*Session, error)
}

func InitSession(user User, storage Storage) (*Session, error) {
	session, err := storage.RecoverSession(user.SessionID)
	if err != nil {
		return nil, err
	}
	if session == nil {
		return storage.CreateSession(user.Name, user.Pass)
	}
	return session, nil
}

type fakeStorage struct {
	createReturns  fakeStorageReturns
	recoverReturns fakeStorageReturns
}

type fakeStorageReturns struct {
	Session *Session
	Err     error
}

func (fs *fakeStorage) RecoverSession(id string) (*Session, error) {
	return nil, nil
	//return fs.recoverReturns.Session, fs.recoverReturns.Err
}

func (fs *fakeStorage) CreateSession(name string, pass string) (Session, error) {
	return Session{ID: "session_id", UserID: 1}, nil
	//return fs.createReturns.Session, fs.createReturns.Err
}

func TestInitSession(t *testing.T) {
	req := require.New(t)
	fs := fakeStorage{}

	cases := map[string]struct {
		user User
	}{
		"user exists":    {user: User{Name: "ivan", Pass: "secret"}},
		"without secret": {user: User{Name: "ivan"}},
		"without name":   {user: User{Pass: "secret"}},
		"empty":          {user: User{}},
	}

	for name, cs := range cases {
		t.Run(name, func(t *testing.T) {
			session, err := InitSession(cs.user, fs)
			req.NoError(err)
			req.Equal("session_id", session.ID)
		})
	}
	t.Run("create session success", func(t *testing.T) {
		req := require.New(t)
		var fs = fakeStorage{
			createReturns: fakeStorageReturns{
				Session: Session{ID: "df", UserID: 2},
				Err:     nil},
			recoverReturns: fakeStorageReturns{Session: nil, Err: nil},
		}
		session, err := InitSession(User{Name: "Ivan", Pass: "secret"}, fs)
		req.NoError(err)
		req.Equal("session_id", session.ID)
	})
}*/
