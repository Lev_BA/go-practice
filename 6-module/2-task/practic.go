package main

import (
	"errors"
	"fmt"
)

type Transport struct {
	Kind       string
	Places     int
	FreePlaces int
}

type Ticket struct {
	Passenger   Pass
	Price       float64
	Kind        string
	Destination string
	Departure   string
}

type Pass struct {
	Name string
	Age  int
}

func NewPass(name string, age int) *Pass {
	return &Pass{
		Name: name,
		Age:  age,
	}
}

func NewTransport(kind string, places int, freePLaces int) *Transport {
	return &Transport{
		Kind:       kind,
		Places:     places,
		FreePlaces: freePLaces,
	}
}

type Options interface {
	RegistrationTicket(pass Pass) (*Ticket, error)
}

func (t *Transport) RegistrationTicket(pass Pass) (*Ticket, error) {
	err := checkFreePlaces(t)
	if err != nil {
		return nil, err
	}

	if t.Kind == "Air" {
		return &Ticket{
			Passenger:   pass,
			Price:       16000,
			Kind:        "Air",
			Destination: "Moscow",
			Departure:   "Minsk",
		}, nil
	}

	if t.Kind == "Bus" {
		return &Ticket{
			Passenger:   pass,
			Price:       4000,
			Kind:        "Bus",
			Destination: "Moscow",
			Departure:   "Minsk",
		}, nil
	}

	return nil, nil
}

func main() {
	checkPass := Pass{"Ivan", 29}
	//checkPass1 := NewPass("Lev", 34)
	checkTransport := NewTransport("Air", 10, 1)

	reg, err := checkTransport.RegistrationTicket(checkPass)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(reg)

}

func checkFreePlaces(tr *Transport) error {
	if tr.FreePlaces == 0 {
		err := errors.New("мест нет")
		return err
	}
	return nil
}
