package main

import (
	"encoding/json"
	_ "github.com/adjust/go-wrk"
	"net/http"
	_ "net/http/pprof"
	"time"
)

type Response struct {
	Numbers []int
}

func handler(w http.ResponseWriter, r *http.Request) {
	a := make([]int, 0)
	b := make([]int, 0)

	for i := 0; i < 300; i++ {
		a = append(a, i)
		b = append(b, i)
	}

	resp := Response{mergeTwoSlices(a, b)}
	jsonData, _ := json.Marshal(resp)
	_, _ = w.Write(jsonData)
}

func mergeTwoSlices(a []int, b []int) []int {
	result := make([]int, 0, len(a)+len(b))
	result = append(result, a...)
	result = append(result, b...)

	return result
}

func CPUHogger() {
	var count uint64
	t := time.Tick(2 * time.Second)
	for {
		select {
		case <-t:
			time.Sleep(50 * time.Microsecond)
		default:
			count++
		}
	}
}

func main() {
	go CPUHogger()
	go CPUHogger()

	http.HandleFunc("/", handler)
	http.ListenAndServe(":8080", nil)
}
