goos: windows
goarch: amd64
pkg: 4task
cpu: Intel(R) Core(TM) i5-3210M CPU @ 2.50GHz
BenchmarkFib-4         	      16	  76630188 ns/op	       0 B/op	       0 allocs/op
BenchmarkFib-4         	      15	  69186140 ns/op	       0 B/op	       0 allocs/op
BenchmarkMakeSlice-4   	     358	   3322344 ns/op	24002568 B/op	       1 allocs/op
BenchmarkMakeSlice-4   	     375	   3221832 ns/op	24002561 B/op	       1 allocs/op
PASS
ok  	4task	5.732s
