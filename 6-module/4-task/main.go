package main

import (
	"fmt"
)

func Fib(n int) int {

	a := 0
	b := 1

	for i := 0; i <= n; i++ {
		a, b = b, a+b
	}
	return a
	/*if n == 0 {
		return 0
	} else if n == 1 {
		return 1
	} else {
		return Fib(n-1) + Fib(n-2)
	}*/
}

func MakeSlice(i int) []int {
	arr := make([]int, 0, i)
	return arr
}

func main() {
	fmt.Println(Fib(10))

}
