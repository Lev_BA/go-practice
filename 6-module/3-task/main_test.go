package main

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestContainsDuplicate(t *testing.T) {
	tests := map[string]struct {
		arr    []string
		result []string
	}{
		"A": {arr: []string{"o", "o", "o", "o"}, result: []string{"о"}},
		"B": {arr: []string{"1", "2", "3", "3"}, result: []string{"1", "2", "3"}},
		"C": {arr: []string{"oдин", "oдин", "два", "два"}, result: []string{"oдин", "два"}},
	}

	for n, v := range tests {
		t.Run(n, func(t *testing.T) {
			req := require.New(t)
			req.Equal(v.result, ContainsDuplicate(v.arr))
		})
	}
}

func TestIsIsPalindrome(t *testing.T) {
	req := require.New(t)
	palindromeCase := func(number int, reverse string) func(t *testing.T) {
		return func(t *testing.T) {
			res := IsPalindrome(number)
			req.Equal(res, reverse)
		}
	}

	t.Run("A", palindromeCase(101, "101"))
	t.Run("A", palindromeCase(1001, "1001"))
	t.Run("A", palindromeCase(99, "99"))
}

/*func TestInitSession(t *testing.T) {
	req := require.New(t)
	any := gomock.Any()

	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	storageMock := mainMock.NewMockStorage(mockCtrl)
	t.Run("create session seccess", func(t *testing.T) {
		storageMock.EXPECT().RecoverSession(any).Return(nil, nil).Times(1)
		storageMock.EXPECT().CreateSession(any, any).Return(&Session{ID: "session_id", UserID: 1}, nil).Times(1)

		session, err := InitSession(User{Name: "ivan", Pass: "secret"}, storageMock)
		req.NoError(err)
		req.Equal("session_id", session.ID)
	})

	t.Run("create session fail", func(t *testing.T) {
		storageMock.EXPECT().RecoverSession(any).Return(nil, nil).Times(1)
		storageMock.EXPECT().CreateSession(any, any).Return(nil, errors.New("fail to create session")).Times(1)
		_, err := InitSession(User{Name: "ivan", Pass: "secret"}, storageMock)
		req.Error(err)
	})

	t.Run("recover session seccess", func(t *testing.T) {
		storageMock.EXPECT().RecoverSession(any).Return(&Session{ID: "session_id", UserID: 1}, nil).Times(1)

		session, err := InitSession(User{Name: "ivan", Pass: "secret"}, storageMock)
		req.Error(err)
		req.Equal("session_id", session.ID)
	})

}*/
