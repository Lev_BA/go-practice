package main

import (
	"strconv"
	"strings"
)

func main() {

}

func ContainsDuplicate(arr []string) []string {
	result := make([]string, 0, len(arr))
	temp := map[string]struct{}{}
	for _, item := range arr {
		if _, ok := temp[item]; !ok {
			temp[item] = struct{}{}
			result = append(result, item)
		}
	}
	return result
}

func IsPalindrome(number int) string {
	typeStringNumber := strconv.Itoa(number)
	a := strings.Split(typeStringNumber, "")
	var arr []string

	for i := len(a) - 1; i >= 0; i-- {
		arr = append(arr, a[i])
	}
	b := strings.Join(arr, "")
	return b
}

/*type User struct {
	SessionID string
	Name      string
	Pass      string
}

type Session struct {
	ID     string
	UserID int
}

type Storage interface {
	RecoverSession(id string) (*Session, error)
	CreateSession(name string, pass string) (*Session, error)
}

func InitSession(user User, storage Storage) (*Session, error) {
	session, err := storage.RecoverSession(user.SessionID)
	if err != nil {
		return nil, err
	}

	if session == nil {
		return storage.CreateSession(user.Name, user.Pass)
	}

	return session, nil
}*/
