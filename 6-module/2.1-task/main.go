package main

import (
	"encoding/xml"
	"errors"
	_ "github.com/golang/mock/gomock"
	"io/ioutil"
	"log"
	"net/http"
)

func main() {

}

/*type myStruct struct{}

func (e *myStruct) MyInitStruct(u string) string {
	return ""
}

type MyInterStruct interface {
	MyInitStruct(u string) string
}

func GetSrting(r string) string {
	gg := r + "aa"
	return gg
}*/

/*var ErrExtractToken = errors.New("failed extract token")

func validate(bearer string, extractor Extractor) error {
	token, err := extractor.Extract(bearer)
	if err != nil {
		return err
	}

	if token == "invalid token" {
		return errors.New("invalid token")
	}

	return nil
}

type Extractor interface {
	Extract(value string) (string, error)
}

type bearerExtractor struct {
	schema string
}

func (ce bearerExtractor) Extract(value string) (string, error) {
	time.Sleep(5 * time.Second)

	if value == "" {
		return "", fmt.Errorf("%w: bearer is empty", ErrExtractToken)
	}

	return "", nil
}*/

type StatusUrl struct {
	Url    string
	Status int
	Valid  int
}

type MyStruct struct {
}

func (ms *MyStruct) MyGetInt() []string {
	arr := make([]string, 0)
	return arr
}

type MyInter interface {
	MyGetInt() []string
}

func DetermineUrlStatus(urls []string) ([]StatusUrl, error) {
	if len(urls) == 0 {
		err := errors.New("empty url list")
		return nil, err
	}
	arr := make([]StatusUrl, 0)

	for _, v := range urls {
		resp, err := http.Get(v)
		if err != nil {
			err := errors.New("not correct url")
			su := StatusUrl{Url: v, Status: 0, Valid: 0}
			arr = append(arr, su)
			log.Printf("error: %s", err)
			continue
		}

		if resp.StatusCode != 200 {
			err := errors.New("error while working with resource")
			su := StatusUrl{Url: v, Status: resp.StatusCode, Valid: 0}
			arr = append(arr, su)
			log.Printf("error: %s", err)
			continue
		}

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			err := errors.New("body contains no data")
			su := StatusUrl{Url: v, Status: resp.StatusCode, Valid: 0}
			arr = append(arr, su)
			log.Printf("error: %s", err)
			continue
		}

		err = xml.Unmarshal(body, new(interface{}))
		if err != nil {
			su := StatusUrl{Url: v, Status: resp.StatusCode, Valid: 0}
			arr = append(arr, su)
			log.Printf("error: %s", err)
			continue
		}

		su := StatusUrl{Url: v, Status: resp.StatusCode, Valid: 100}
		arr = append(arr, su)
		log.Println(su)
	}

	return arr, nil
}
