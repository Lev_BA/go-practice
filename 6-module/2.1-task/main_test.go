package main

import (
	"fmt"
	"github.com/golang/mock/gomock"
	mock_main "testUrl/mocks"
	"testing"
)

func TestDetermineUrlStatus(t *testing.T) {
	mockController := gomock.NewController(t)
	defer mockController.Finish()

	mockData := mock_main.NewMockMyInter(mockController)
	mockData.EXPECT().MyGetInt().Times(1)

	_, err := DetermineUrlStatus(mockData.MyGetInt())

	fmt.Println(err)
}

/*func TestGetSrting(t *testing.T) {
	req := require.New(t)

	mockController := gomock.NewController(t)
	defer mockController.Finish()

	mockStrict := mock_main.NewMockmyInterStruct(mockController)
	mockStrict.EXPECT().MyInitStruct(gomock.Any()).Return("good").Times(1)

	spr := GetSrting(mockStrict.MyInitStruct(""))
	req.Equal("goodaa", spr)
}*/

/*func TestValidate(t *testing.T) {
	req := require.New(t)

	mockController := gomock.NewController(t)
	defer mockController.Finish()

	mockExtractor := mock_main.NewMockExtractor(mockController)

	mockExtractor.EXPECT().Extract(gomock.Any()).Return("Token", nil).Times(1)
	err := validate("bearer sss", mockExtractor)
	req.NoError(err)
}*/

/*type HttpService interface {
	Get(url string) StatusUrl
}

type ProdService struct {
}

func (s *ProdService) Get(url string) StatusUrl {
	resp, err := http.Get(url)
	println(resp)
	println(err)
	return StatusUrl{}
}

type TestService struct {
}

func (s *TestService) Get(url string) (StatusUrl, error) {
	return StatusUrl{
		Url:    "",
		Status: 1,
		Valid:  1,
	}, nil
}

type Checker struct {
	httpService HttpService
}

func New(s HttpService) *Checker {
	return &Checker{
		httpService: s,
	}
}

func (s *Checker) Run() {
	//for
	//status := s.httpService.Get("")
}

s := &build.ProdService{}
c := build.New(s)
c.Run()

//
s1 := &build.TestService{}
c1 := build.New(s1)
c1.Run()*/

/*type Cache interface {
	Get(key string) (string, error)
	Set(key, value string) error
}

type MockCache struct {
	GetFn func(key string) (string, error)
	SetFn func(key, value string) error
}

func (m *MockCache) Get(key string) (string, error) {
	return m.GetFn(key)
}

func (m *MockCache) Set(key, value string) error {
	return m.SetFn(key, value)
}

type Tests struct {
	Name     string
	Server   *httptest.Server
	Response *StatusUrl
	Error    error
}

func TestDetermineUrlStatus(t *testing.T) {
	//_, err := DetermineUrlStatus(c1)

	tests := []Tests{
		{
			Name: "basic-request",
			Server: httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusOK)
				w.Write([]byte(`{ <note><to>Вася</to><from>Света</from><heading>Напоминание</heading><body>Позвони мне завтра!</body></note> }`))
			})),
			Response: &StatusUrl{
				Url:    "url",
				Status: 200,
				Valid:  100,
			},
			Error: nil,
		},
	}

	for _, test := range tests {
		t.Run(test.Name, func(t *testing.T) {
			defer test.Server.Close()

			resp, err := DetermineUrlStatus([]string{test.Server.URL})

			if !errors.Is(err, test.Error) {
				t.Errorf("failed: expected %v, got %v\n", test.Error, err)
			}

			t.Logf("Result: %v", resp)
		})
	}
}*/
