package utils

import "testing"

func TestReverseInt(t *testing.T) {
	t.Run("test1", func(t *testing.T) {
		number := 123
		result := ReverseInt(number)

		if number == result {
			t.Error("Ошибка в расчете числа")
		}
	})
	t.Run("test2", func(t *testing.T) {
		number := -649
		result := ReverseInt(number)

		if number == result {
			t.Error("Ошибка в расчете числа")
		}
	})
	t.Run("test3", func(t *testing.T) {
		number := 0
		result := ReverseInt(number)

		if number == result {
			t.Error("Ошибка в расчете числа")
		}
	})

}
