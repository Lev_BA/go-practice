package main

import (
	"fmt"
	"math"
)

func main() {

	x1 := ReverseInt(-123456789)

	fmt.Println(x1)

}

func ReverseInt(x interface{}) int {
	num := x.(int)

	sign := "positive"
	if num >= 0 {
		sign = "positive"
	} else {
		sign = "negative"
	}

	num = int(math.Abs(float64(num)))

	var reversedDigit int
	for num > 0 {
		lastDigit := num % 10
		reversedDigit = reversedDigit*10 + lastDigit
		num = num / 10
	}

	if sign == "negative" {
		reversedDigit = reversedDigit * -1
	}
	return reversedDigit
}
